import uuid
import factory
from faker import Faker
import random

from django.conf import settings
from .factories_common import BaseModelFactory

from apps.users.models import ProfileUser


User = settings.AUTH_USER_MODEL
TEST_USER_PASSWORD = uuid.uuid4().hex
fake = Faker()


class UserFactory(BaseModelFactory):
    username = factory.Sequence(lambda n: 'username_{}'.format(n))
    email = factory.Sequence(lambda n: 'email{}gmail.com'.format(n))
    password = factory.PostGenerationMethodCall('set_password', TEST_USER_PASSWORD)

    class Meta:
        model = User


# help function for choices field
class ModelFieldLazyChoice(factory.LazyFunction):
    def __init__(self, model_class, field, *args, **kwargs):
        choices = [choice[1] for choice in model_class._meta.get_field(field).choices]
        super(ModelFieldLazyChoice, self).__init__(
            # function=lambda: random.choice(choices),
            function=lambda: choices[0],
            *args, **kwargs
        )

class ProfileUserFactory(BaseModelFactory):
    class Meta:
        model = ProfileUser

    user = factory.SubFactory(UserFactory)
    person = ModelFieldLazyChoice(ProfileUser, 'person')