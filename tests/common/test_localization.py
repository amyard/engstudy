from django.test import TestCase
from django.urls import reverse
from django.utils.translation import activate
from django.utils.translation import ugettext_lazy as _

from apps.books.middleware import switch_lang_code

class TestHomePage(TestCase):

    def test_uses_en_language(self):
        activate('en')
        response = self.client.get(reverse("books:base_view"))
        self.assertTemplateUsed(response, "base.html")

    def test_uses_ru_language(self):
        activate('ru')
        response = self.client.get(reverse("books:base_view"))
        self.assertTemplateUsed(response, "base.html")

    def test_translate_by_en(self):
        activate('en')
        self.assertEqual(_('translate this'), 'Translate This')

    def test_translate_ru(self):
        activate('ru')
        self.assertEqual(_('translate this'), 'Переведи это')


class LangTestUrlsNewMiddleware(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.list_url = reverse('lesson_tests:lesson_tests_list')
        cls.base_url = reverse('books:base_view')

    def test_default_url(self):
        response = self.client.get(self.list_url)
        curr_url = response.context[0].request.get_full_path()
        self.assertFalse('/ru' in curr_url)

    def test_url_change_to_eng_lang_listview(self):
        lang = 'en'
        response = self.client.get(self.list_url)
        activate(lang)
        curr_url = response.context[0].request.get_full_path()
        ans = switch_lang_code(curr_url, lang)
        self.assertTrue('/en' in ans)

    def test_url_change_eng_to_ru_lang_listview(self):
        lang = 'en'
        response = self.client.get(self.list_url)
        activate(lang)
        curr_url = response.context[0].request.get_full_path()
        ans = switch_lang_code(curr_url, lang)
        self.assertTrue('/en' in ans)

        lang2 = 'ru'
        response = self.client.get(self.list_url)
        activate(lang2)
        curr_url = response.context[0].request.get_full_path()
        ans2 = switch_lang_code(curr_url, lang2)
        self.assertFalse('/en' in ans2)
        self.assertFalse('/ru' in ans2)

    def test_url_baseview(self):
        response = self.client.get(self.base_url)
        curr_url = response.context[0].request.get_full_path()
        self.assertEqual(curr_url, '/')
        self.assertFalse('/ru' in curr_url)
        self.assertFalse('/en' in curr_url)

    def test_url_change_to_eng_lang_baseview(self):
        lang = 'en'
        response = self.client.get(self.base_url)
        activate(lang)
        curr_url = response.context[0].request.get_full_path()
        ans = switch_lang_code(curr_url, lang)
        self.assertTrue('/en' in ans)
        self.assertFalse('/ru' in ans)
        self.assertEqual(ans, '/en/')

    def test_url_change_eng_to_ru_lang_baseview(self):
        lang = 'en'
        response = self.client.get(self.base_url)
        activate(lang)
        curr_url = response.context[0].request.get_full_path()
        ans = switch_lang_code(curr_url, lang)
        self.assertTrue('/en' in ans)
        self.assertFalse('/ru' in ans)
        self.assertEqual(ans, '/en/')

        lang2 = 'ru'
        response = self.client.get(self.base_url)
        activate(lang2)
        curr_url = response.context[0].request.get_full_path()
        ans2 = switch_lang_code(curr_url, lang2)
        self.assertFalse('/en' in ans2)
        self.assertFalse('/ru' in ans2)
        self.assertEqual(curr_url, '/')