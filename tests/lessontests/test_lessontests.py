import sys
sys.path.append("..")


from django.db import IntegrityError
from django.core.exceptions import ValidationError, NON_FIELD_ERRORS
from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.urls import reverse, resolve
from django.utils.text import slugify
from django.test import TestCase, Client
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File
from django.conf import settings
import os
import mock


from tests.factories_users import UserFactory, ProfileUserFactory
from tests.factories_lessontests import LessonTestsFactory, TaskTestsFactory, ResultTestModelFactory
from apps.lessontests.models import LessonTests, TaskTests, ResultTestModel
from apps.lessontests.forms import LessonTestsCreateForm, LessonTestsUpdateForm

from django.template import Context, Template
from apps.lessontests.templatetags.lessontags import display_individ_lesson

User = get_user_model()

# class LessonTestsModelTestCase(TestCase):

#     def setUp(self):
#         self.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         self.profile = ProfileUserFactory(user = self.user)
#         self.lessontests = LessonTestsFactory(owner=self.profile)

#         self.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         self.profile2 = ProfileUserFactory(user = self.user)

#     def test_slug_field_automatically(self):
#         slug = self.lessontests.title.replace(' ', '-').lower()
#         self.assertEqual(self.lessontests.slug, slug)

#     def test_slug_field_slugify(self):
#         slug = slugify(self.lessontests.title, allow_unicode = True)
#         self.assertEqual(self.lessontests.slug, slug)

#     def test_choices_default(self):
#         self.assertEqual(self.lessontests.permission, 'All')
    
#     def test_choices_change_inviduals(self):
#         lessontests = LessonTestsFactory(owner = self.profile, permission = 'Individual')
#         self.assertEqual(lessontests.permission, 'Individual')
    
#     def test_str_field(self):
#         self.assertEqual(self.lessontests.__str__(), self.lessontests.title)

#     def test_check_unique_together_two_diff_persons(self):
#         less1 = LessonTestsFactory(title='test 1', owner = self.profile)
#         less2 = LessonTestsFactory(title='test 1', owner = self.profile2)
#         less3 = LessonTestsFactory(title='test 2', owner = self.profile2)

#         count_data = LessonTests.objects.filter(title='test 1').count()
#         self.assertEqual(count_data, 2)

#     def test_check_unique_together_invalid(self):
#         LessonTestsFactory(title='test 1', slug='test-1', owner = self.profile)
#         LessonTestsFactory(title='test 1', slug='test-1', owner = self.profile2)

#         with self.assertRaises(IntegrityError):
#             LessonTestsFactory(title='test 1', slug='test-1', owner = self.profile2)




# class TaskTestsModelTestCase(TestCase):

#     def setUp(self):
#         self.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         self.profile = ProfileUserFactory(user = self.user)
#         self.lessontests = LessonTestsFactory(owner=self.profile)
#         self.tasktest = TaskTestsFactory(lesson=self.lessontests)

#     def test_slug_field_automatically(self):
#         slug = self.tasktest.title.replace(' ', '-').lower()
#         self.assertEqual(self.tasktest.slug, slug)

#     def test_slug_field_slugify(self):
#         slug = slugify(self.tasktest.title, allow_unicode = True)
#         self.assertEqual(self.tasktest.slug, slug)

#     def test_choices_right_answer_default_answer_a(self):
#         self.assertEqual(self.tasktest.right_answer, 'Answer A')
    
#     def test_change_right_answer_to_another(self):
#         tasktest = TaskTestsFactory(lesson = self.lessontests, right_answer = self.tasktest.Answers[1][1])
#         self.assertEqual(tasktest.right_answer, 'Answer B')
    
#     def test_str_field(self):
#         self.assertEqual(self.tasktest.__str__(), f'{self.tasktest.id}-{self.tasktest.lesson.title}')

#     def test_default_type_of_question(self):
#         self.assertEqual(self.tasktest.type_question, 'Выберите правильный ответ')

#     def test_change_type_of_question(self):
#         self.tasktest.type_question = 'Type answer'
#         self.tasktest.save()
#         self.lessontests.refresh_from_db()
#         get_data = TaskTests.objects.first()
#         self.assertEqual(get_data.type_question, 'Type answer')

#     # def test_get_error_create_type_type(self):
#     #     t = TaskTests(lesson=self.lessontests, right_answer = 'a',
#     #                         type_question='audio', 
#     #                         question = 'asd ... asd ... asd', 
#     #                         custom_right_answer = 'dsadsa')
#     #     t.save()
#     #     try:
#     #         t.full_clean()
#     #     except ValidationError as e:
#     #         non_field_errors = e.message_dict[NON_FIELD_ERRORS]
#         # t.refresh_from_db()

#         # tt = TaskTests.objects.filter(lesson=self.lessontests)
#         # tt.type_question = 'type'
        
#         # with self.assertRaises(ValidationError):
#         #     tt.save()
#         #     print(t)
#         #     t.full_clean()

#         # instance = TaskTests(
#         #                     type_question='move', 
#         #                     question = 'asd ... asd ... asd', 
#         #                     custom_right_answer = 'dsadsa, asds, sadsa'
#         # )
#         # self.assertRaises(ValidationError, instance.clean)



# class CustomFieldTestCase(TestCase):

#     def setUp(self):
#         self.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         self.profile = ProfileUserFactory(user = self.user)
#         self.lessontests = LessonTestsFactory(owner=self.profile)

#     def test_empty_field(self):
#         tt = TaskTests.objects.create(
#             lesson=self.lessontests,
#             question = 'asda sda sd  ad a'
#         )
#         tt.refresh_from_db()
#         self.assertEqual(TaskTests.objects.get(lesson=self.lessontests).audio_file, '')
    
#     def test_fiel_field_with_correct_data(self):
#         file_mock = mock.MagicMock(spec=File)
#         file_mock.name = '12345.mp3'
#         tt = TaskTests(
#             lesson=self.lessontests,
#             question = 'asda sda sd  ad a',
#             audio_file = file_mock
#         )
#         tt.save()

#         self.assertIn('audio', tt.audio_file.url)
#         self.assertIn(tt.lesson.slug, tt.audio_file.url)
#         self.assertNotEqual(tt.audio_file.url.split('/')[-1], file_mock.name)

#     # def test_file_field_with_correct_data(self):
#     #     file_mock = mock.MagicMock(spec=File)
#     #     file_mock.name = 'img.jpg'
#     #     tt = TaskTests(
#     #         lesson=self.lessontests,
#     #         question = 'asda sda sd  ad a',
#     #         audio_file = file_mock
#     #     )
#     #     tt.save()

#     #     print(tt.audio_file)
#     #     print(tt.audio_file.url)

# class TaskTestsPositionTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.profile = ProfileUserFactory(user = cls.user)
#         cls.lesson1 = LessonTestsFactory(title='Lesson 1', owner=cls.profile)
#         cls.lesson2 = LessonTestsFactory(title='Lesson 2', owner=cls.profile)

#     def test_create_tasttest_with_default_position(self):
#         obj = TaskTests.objects.create(
#             title = 'Test 1',
#             lesson = self.lesson1
#         )
#         self.assertEqual(TaskTests.objects.last().position_in, 1)

#     def test_create_tasttest_with_add_custom_position(self):
#         obj = TaskTests.objects.create(
#             title = 'Test 1',
#             lesson = self.lesson1,
#             position_in = 5
#         )
#         self.assertEqual(TaskTests.objects.last().position_in, 5)

#     def test_generate_automatically_title_with_default_position(self):
#         TaskTests.objects.create(lesson = self.lesson1)
#         self.assertEqual(TaskTests.objects.last().title, 'Задание №1')

#     def test_generate_automatically_title_with_custom_position(self):
#         TaskTests.objects.create(
#             lesson = self.lesson1,
#             position_in = 5
#         )
#         self.assertEqual(TaskTests.objects.last().title, 'Задание №5')

#     def test_create_generate_custom_data_with_default_position(self):
#         t1 = TaskTests.objects.create(title = '111', lesson = self.lesson1)
#         t2 = TaskTests.objects.create(title = '2222', lesson = self.lesson1)
#         t4 = TaskTests.objects.create(title = '4444', lesson = self.lesson1)
#         data = TaskTests.objects.filter(lesson=self.lesson1).order_by('id').values_list('position_in', flat=True)
#         self.assertSequenceEqual(data, [1,2,3])

#     def test_create_generate_custom_data_with_custom_position(self):
#         t1 = TaskTests.objects.create(position_in=1, lesson = self.lesson1)
#         t2 = TaskTests.objects.create(position_in=3, lesson = self.lesson1)
#         t4 = TaskTests.objects.create(position_in=6, lesson = self.lesson1)
#         data = TaskTests.objects.filter(lesson=self.lesson1).order_by('id').values_list('position_in', flat=True)
#         self.assertSequenceEqual(data, [1,3,6])
    
#     def test_change_all_postions_after_repeat_position(self):
#         t1 = TaskTests.objects.create(lesson = self.lesson1)
#         t2 = TaskTests.objects.create(lesson = self.lesson1)
#         t3 = TaskTests.objects.create(lesson = self.lesson1, title = 'Old Pos 3')
#         t4 = TaskTests.objects.create(lesson = self.lesson1, title = 'Old Pos 4')

#         data = TaskTests.objects.filter(lesson=self.lesson1).order_by('id').values_list('position_in', flat=True)
#         self.assertSequenceEqual(data, [1,2,3,4])

#         ct = TaskTests.objects.create(lesson = self.lesson1, position_in = 2, title='New title')
#         self.assertEqual(TaskTests.objects.get(title='New title').position_in, 2)
#         self.assertEqual(TaskTests.objects.get(title='Old Pos 3').position_in, 4)
#         self.assertEqual(TaskTests.objects.get(title='Old Pos 4').position_in, 5)


# class LessonTestsListViewTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.profile = ProfileUserFactory(user = cls.user)
#         cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         cls.profile2 = ProfileUserFactory(user = cls.user2)
        
#         cls.url = reverse('lesson_tests:lesson_tests_list')

    
#     def test_display_no_data_for_anonymous_user(self):
#         response = self.client.get(self.url)
#         curr_user = response.context["user"]
#         obj = LessonTests.objects.all().exists()

#         self.assertEqual(curr_user, AnonymousUser())
#         self.assertEqual(response.status_code, 200)
#         assert 'base.html' in [t.name for t in response.templates]
#         assert 'lessontests/lessontest_list.html' in [t.name for t in response.templates]
#         self.assertContains(response, 'Извините, уроки отсутствуют.')
#         self.assertFalse(obj)
#         self.assertQuerysetEqual(response.context['object_list'], [])
    
#     def test_display_no_data_for_logged_user(self):
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#         curr_user = response.wsgi_request.user
#         obj = LessonTests.objects.all().exists()

#         self.assertEqual(curr_user, self.user)
#         self.assertEqual(response.status_code, 200)
#         assert 'base.html' in [t.name for t in response.templates]
#         assert 'lessontests/lessontest_list.html' in [t.name for t in response.templates]
#         self.assertContains(response, 'Извините, уроки отсутствуют.')
#         self.assertFalse(obj)
#         self.assertQuerysetEqual(response.context['object_list'], [])

#     def test_display_data_anonym_user(self):
#         LessonTestsFactory.create_batch(5, owner=self.profile)
#         response = self.client.get(self.url)
#         curr_user = response.context["user"]

#         self.assertEqual(curr_user, AnonymousUser())
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(LessonTests.objects.all().count(), 5)

#     def test_display_data_logged_user(self):
#         LessonTestsFactory.create_batch(5, owner=self.profile)
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#         curr_user = response.wsgi_request.user

#         self.assertEqual(curr_user, self.user)
#         self.assertEqual(response.status_code, 200)
#         self.assertEqual(LessonTests.objects.count(), 5)

#     def test_display_data_different_created_users(self):
#         LessonTestsFactory.create_batch(5, owner=self.profile)
#         LessonTestsFactory.create_batch(5, owner=self.profile2)
#         response = self.client.get(self.url)

#         self.assertEqual(LessonTests.objects.count(), 10)   

#     def test_display_data_custom_manager(self):
#         LessonTestsFactory.create_batch(2, owner=self.profile)
#         LessonTestsFactory.create_batch(2, owner=self.profile2)
#         LessonTestsFactory.create_batch(2, owner=self.profile2, permission='A')
    
#         response = self.client.get(self.url)
#         self.assertEqual(LessonTests.all_objects.count(), 2)

#     def test_count_tasks_by_lesson_all(self):
#         ls1 = LessonTestsFactory(owner=self.profile, permission='A')
#         ls2 = LessonTestsFactory(owner=self.profile2, permission='A')
#         ls3 = LessonTestsFactory(owner=self.profile, permission='I')

#         TaskTestsFactory.create_batch(4, lesson = ls1)
#         TaskTestsFactory.create_batch(2, lesson = ls2)
#         TaskTestsFactory.create_batch(5, lesson = ls3)

#         response = self.client.get(self.url)
        
#         res = sum([i['count'] for i in response.context[0]['tasks']])
#         self.assertEqual(res, 6)



# class LessonTestsListViewIndividualTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.profile = ProfileUserFactory(user = cls.user, person = 'Teacher')
#         cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         cls.profile2 = ProfileUserFactory(user = cls.user2)

#         cls.user3 = UserFactory(username='testing3', email='testing3@gmail.com', password='zaza1234')
#         cls.student = ProfileUserFactory(user = cls.user3, person = 'Student')
#         cls.user4 = UserFactory(username='testing4', email='testing4@gmail.com', password='zaza1234')
#         cls.student2 = ProfileUserFactory(user = cls.user4, person = 'Student')
#         cls.user5 = UserFactory(username='testing5', email='testing5@gmail.com', password='zaza1234')
#         cls.student3 = ProfileUserFactory(user = cls.user5, person = 'Student')
        
#         cls.url = reverse('lesson_tests:lesson_tests_list_individual')

    
#     def test_display_no_data_for_anonymous_user(self):
#         response = self.client.get(self.url)
#         curr_user = response.context["user"]
#         obj = LessonTests.objects.all().exists()

#         self.assertEqual(curr_user, AnonymousUser())
#         self.assertEqual(response.status_code, 200)
#         assert 'base.html' in [t.name for t in response.templates]
#         assert 'lessontests/lessontest_list_individ.html' in [t.name for t in response.templates]
#         self.assertContains(response, 'Извините, уроки отсутствуют.')
#         self.assertFalse(obj)
#         self.assertQuerysetEqual(response.context['object_list'], [])
    
#     def test_display_no_data_for_logged_user(self):
#         self.client.force_login(self.user)
#         response = self.client.get(self.url)
#         curr_user = response.wsgi_request.user
#         obj = LessonTests.objects.all().exists()

#         self.assertEqual(curr_user, self.user)
#         self.assertEqual(response.status_code, 200)
#         assert 'base.html' in [t.name for t in response.templates]
#         assert 'lessontests/lessontest_list_individ.html' in [t.name for t in response.templates]
#         self.assertContains(response, 'Извините, уроки отсутствуют.')
#         self.assertFalse(obj)
#         self.assertQuerysetEqual(response.context['object_list'], [])



# class LessonListIndividTemplatagTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.client = Client()

#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234', is_active=True)
#         cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         cls.user3 = UserFactory(username='testing3', email='testing3@gmail.com', password='zaza1234')
#         cls.user4 = UserFactory(username='testing4', email='testing4@gmail.com', password='zaza1234')
#         cls.user5 = UserFactory(username='testing5', email='testing5@gmail.com', password='zaza1234')
#         cls.user6 = UserFactory(username='testing6', email='testing6@gmail.com', password='zaza1234', is_admin=True, is_superuser=True, is_staff=True, is_active=True)

#         cls.owner = ProfileUserFactory(user = cls.user, person = 'Teacher')
#         cls.teacher = ProfileUserFactory(user = cls.user2, person = 'Teacher')        
#         cls.student = ProfileUserFactory(user = cls.user3, person = 'Student')
#         cls.student2 = ProfileUserFactory(user = cls.user4, person = 'Student')
#         cls.student3 = ProfileUserFactory(user = cls.user5, person = 'Student')
#         cls.admin = ProfileUserFactory(user = cls.user6, person = 'Student')

#         cls.url = reverse('lesson_tests:lesson_tests_list_individual')
#         cls.lessontests = LessonTestsFactory(title = 'test test', owner=cls.owner, permission='I', permit = [cls.student2, cls.student3])


#     def test_anonynous_user(self):
#         response = self.client.get(self.url)
#         curr_user = response.context["user"]

#         self.assertEqual(curr_user, AnonymousUser())
#         self.assertEqual(response.status_code, 200)
#         assert 'base.html' in [t.name for t in response.templates]
#         assert 'lessontests/lessontest_list_individ.html' in [t.name for t in response.templates]
#         self.assertContains(response, 'Извините, уроки отсутствуют.')
#         self.assertQuerysetEqual(response.context['object_list'], [])

#     def test_admin_display_display_individ_lesson(self):
#         result = display_individ_lesson(self.admin.user)
#         self.assertTrue(result)
#         self.assertEqual(result[0].title, self.lessontests.title)

#     def test_owner_display_display_individ_lesson(self):
#         # t = Template("{% load lessontags %} {% display_individ_lesson user as lessons %}")
#         # c = Context({'user': self.owner.user})
#         # rendered = t.render(c)

#         result = display_individ_lesson(self.owner.user)
#         self.assertTrue(result)
#         self.assertEqual(result[0].title, self.lessontests.title)

#     def test_another_teacher_display_display_individ_lesson(self):
#         result = display_individ_lesson(self.teacher.user)
#         self.assertFalse(result)
#         self.assertEqual(len(result), 0)

#     def test_permit_student_display_display_individ_lesson(self):
#         result = display_individ_lesson(self.student2.user)
#         self.assertTrue(result)
#         self.assertEqual(result[0].title, 'test test')

#     def test_not_permit_student_display_display_individ_lesson(self):
#         result = display_individ_lesson(self.student.user)
#         self.assertFalse(result)
#         self.assertEqual(len(result), 0)

    
# class LessonTestsCreateViewTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.client = Client()
#         cls.url = reverse('lesson_tests:lesson_tests_create')
#         cls.redirect_login = '/users/login/?next=/lesson_tests/lesson/create/'
#         cls.redirect_list = reverse('lesson_tests:lesson_tests_list')
#         cls.form_data = {'title':'New Lesson', 'permission':'A'}

#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.teacher = ProfileUserFactory(user = cls.user, person = 'Teacher')
#         cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         cls.student = ProfileUserFactory(user = cls.user2, person = 'Student')
#         cls.user3 = UserFactory(username='testing3', email='testing3@gmail.com', password='zaza1234', is_admin=True, is_superuser=True, is_staff=True, is_active=True)
#         cls.admin = ProfileUserFactory(user = cls.user3, person = 'Student')
        
    
#     def test_create_view_anonymous_user(self):
#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_login, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
    
#     def test_create_view_student_user(self):
#         self.client.force_login(self.student.user)
#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 403)
    
#     # def test_create_view_admin_permission(self):
#     #     self.client.force_login(self.admin.user)
#     #     response = self.client.get(self.url)
#     #     self.assertEqual(response.status_code, 200)

#     #     response = self.client.post(self.url, self.form_data)
#     #     self.assertEqual(response.status_code, 302)
#     #     dt = LessonTests.objects.count()
#     #     self.assertEqual(dt, 1)
#     #     self.assertRedirects(response, self.redirect_list, status_code=302,
#     #                          target_status_code=200, fetch_redirect_response=True)
    
#     # def test_create_view_teacher_user(self):
#     #     self.client.force_login(self.teacher.user)
#     #     response = self.client.get(self.url)
#     #     self.assertEqual(response.status_code, 200)

#     #     form = {'title':'New Lesson', 'permission':'A'}
#     #     response = self.client.post(self.url, form, {'owner':self.teacher})
#     #     self.assertEqual(response.status_code, 302)
#     #     dt = LessonTests.objects.count()
#     #     self.assertEqual(dt, 1)
#     #     self.assertRedirects(response, self.redirect_list, status_code=302,
#     #                          target_status_code=200, fetch_redirect_response=True)

#     def test_create_form_valid(self):
#         form = LessonTestsCreateForm(data = {'title':'laallall', 'permission':'A'})
#         self.assertTrue(form.is_valid())
    
#     def test_create_form_invalid(self):
#         form = LessonTestsCreateForm(data = {'title':'laallall'})
#         self.assertFalse(form.is_valid())
    
#     def test_create_form_valid_was_not_created(self):
#         form = LessonTestsCreateForm(data = {'title':'laallall', 'permission':'A'})
#         obj_count = LessonTests.objects.count()
#         self.assertEqual(obj_count, 0)

#     # def test_create_form_valid_was_created(self):
#     #     self.client.force_login(self.teacher.user)
#     #     response = self.client.get(self.url)

#     #     form = LessonTestsCreateForm(data = {'title':'laallall', 'permission':'A'})
#     #     if form.is_valid():
#     #         form.save()
#     #         obj_count = LessonTests.objects.count()
#     #         self.assertEqual(obj_count, 1)


#     # def test_create_form_generate_slug_and_user_auto(self):
#     #     self.client.force_login(self.teacher.user)
#     #     response = self.client.get(self.url)

#     #     form = LessonTestsCreateForm(data = {'title':'laallall', 'permission':'A'})
#     #     if form.is_valid():
#     #         form.save()
#     #         obj = LessonTests.objects.last()
#     #         self.assertEqual(obj.slug, slugify(obj.title, allow_unicode = True))


# class LessonTestsUpdateViewTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.client = Client()
#         cls.form_data = {'title':'New Lesson', 'permission':'I'}
   
#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.teacher = ProfileUserFactory(user = cls.user, person = 'Teacher')
#         cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         cls.student = ProfileUserFactory(user = cls.user2, person = 'Student')
#         cls.user3 = UserFactory(username='testing3', email='testing3@gmail.com', password='zaza1234', is_admin=True, is_superuser=True, is_staff=True, is_active=True)
#         cls.admin = ProfileUserFactory(user = cls.user3, person = 'Student')

#         cls.user4 = UserFactory(username='testing4', email='testing4@gmail.com', password='zaza1234')
#         cls.owner = ProfileUserFactory(user = cls.user4, person = 'Teacher')

#         cls.lessontests = LessonTestsFactory(owner=cls.owner)
#         cls.url = reverse('lesson_tests:lesson_tests_update', kwargs = {'pk':cls.lessontests.pk, 'slug':cls.lessontests.slug})
#         cls.redirect_login = f'/users/login/?next=/lesson_tests/lesson/update/{cls.lessontests.pk}-{cls.lessontests.slug}'
#         cls.redirect_detail = reverse('lesson_tests:lesson_tests_detail', args = [cls.lessontests.pk, 'new-lesson'])
    
#     def test_update_view_anonymous_user(self):
#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_login, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
    
#     def test_update_view_student_user(self):
#         self.client.force_login(self.student.user)
#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 403)
    
#     def test_update_view_another_teacher_user(self):
#         self.client.force_login(self.student.user)
#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 403)

    
#     def test_update_view_admin_permission(self):
#         self.client.force_login(self.admin.user)
#         response = self.client.get(self.url)
#         self.assertEqual(response.status_code, 200)

#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_detail, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
#         self.lessontests.refresh_from_db()
#         self.assertEqual(self.lessontests.title, 'New Lesson')

#     def test_update_view_owner_permission(self):
#         self.client.force_login(self.owner.user)
#         response = self.client.get(self.url)
#         self.assertEqual(response.status_code, 200)

#         response = self.client.post(self.url, self.form_data)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_detail, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
#         self.lessontests.refresh_from_db()
#         self.assertEqual(self.lessontests.title, 'New Lesson')


# class LessonTestsUpdateViewTestCase(TestCase):

#     @classmethod
#     def setUpTestData(cls):
#         cls.client = Client()

#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.teacher = ProfileUserFactory(user = cls.user, person = 'Teacher')
#         cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
#         cls.student = ProfileUserFactory(user = cls.user2, person = 'Student')
#         cls.user3 = UserFactory(username='testing3', email='testing3@gmail.com', password='zaza1234', is_admin=True, is_superuser=True, is_staff=True, is_active=True)
#         cls.admin = ProfileUserFactory(user = cls.user3, person = 'Student')

#         cls.user4 = UserFactory(username='testing4', email='testing4@gmail.com', password='zaza1234')
#         cls.owner = ProfileUserFactory(user = cls.user4, person = 'Teacher')

#         cls.lessontests = LessonTestsFactory(owner=cls.owner)
#         cls.url = reverse('lesson_tests:lesson_tests_delete', kwargs = {'pk':cls.lessontests.pk})
#         cls.redirect_login = f'/users/login/?next=/lesson_tests/lesson/delete/{cls.lessontests.pk}'
#         cls.redirect_list = reverse('lesson_tests:lesson_tests_list')


#     def test_delete_view_anonymous_user(self):
#         response = self.client.post(self.url)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_login, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
    
#     def test_delete_view_student_user(self):
#         self.client.force_login(self.student.user)
#         response = self.client.post(self.url)
#         self.assertEqual(response.status_code, 403)
    
#     def test_delete_view_another_teacher_user(self):
#         self.client.force_login(self.student.user)
#         response = self.client.post(self.url)
#         self.assertEqual(response.status_code, 403)
    
#     def test_delete_view_admin_permission(self):
#         self.client.force_login(self.admin.user)
#         response = self.client.post(self.url)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_list, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
#         self.assertEqual(LessonTests.objects.count(), 0)



#     def test_delete_view_owner_permission(self):
#         self.client.force_login(self.owner.user)
#         response = self.client.post(self.url)
#         self.assertEqual(response.status_code, 302)
#         self.assertRedirects(response, self.redirect_list, status_code=302,
#                              target_status_code=200, fetch_redirect_response=True)
#         self.assertEqual(LessonTests.objects.count(), 0)

#     def test_delete_view_get_function_with_user(self):
#         self.client.force_login(self.owner.user)
#         response = self.client.get(self.url)
#         self.assertEqual(response.status_code, 200)

#         self.assertContains(response, 'Вы действительно хотите удалить')
#         assert 'base.html' in [t.name for t in response.templates]
#         assert 'lessontests/lessontest_delete.html' in [t.name for t in response.templates]



# class TestResultTestModel(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cls.client = Client()
#         cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         cls.teacher = ProfileUserFactory(user = cls.user, person = 'Teacher')
#         cls.lessontests = LessonTestsFactory(owner=cls.teacher, title='new test for test')

#     def test_create_model(self):
#         obj = ResultTestModel(
#             lesson = self.lessontests,
#             user = self.teacher,
#             result = 'fggfgdfg fdgfd gdfg dfg dfg df gdf dfg dfdf gdf gdf g'
#         )
#         obj.save()
#         obj.refresh_from_db()
#         res = ResultTestModel.objects.count()
#         self.assertEqual(res, 1)
#         self.assertEqual(ResultTestModel.objects.first().__str__(), '{}-{}'.format(self.lessontests, self.teacher))






from django.dispatch import Signal
from mock_django.signals import mock_signal_receiver
from django.db.models import signals

from apps.lessontests.models import delete_img_pre_delete_post

# class CustomFieldTestCase(TestCase):

#     def setUp(self):
#         self.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
#         self.profile = ProfileUserFactory(user = self.user)
#         self.lessontests = LessonTestsFactory(owner=self.profile)
    
#     def test_signal_registry(self):
#         registered_functions = [r[1]() for r in signals.pre_delete.receivers]
#         self.assertIn(delete_img_pre_delete_post, registered_functions)

#     def test_mock_receiver(self):
#         signal = Signal()
#         with mock_signal_receiver(signal) as receiver:
#             signal.send(sender=None)
#             self.assertEqual(receiver.call_count, 1)

#         sentinel = {}

#         def side_effect(*args, **kwargs):
#             return sentinel

#         with mock_signal_receiver(signal, wraps=side_effect) as receiver:
#             responses = signal.send(sender=None)
#             self.assertEqual(receiver.call_count, 1)

#             # Signals respond with a list of tuple pairs [(receiver, response), ...]
#             self.assertIs(responses[0][1], sentinel)



from apps.lessontests.views import AjaxGetAnswerFromTask
import json
class AjaxGetAnswerFromTaskTestCase(TestCase):
    
    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.url = reverse('lesson_tests:get_task_answer')

        cls.user = UserFactory(username='testing', email='testing@gmail.com', password='zaza1234')
        cls.profile = ProfileUserFactory(user = cls.user)
        cls.user2 = UserFactory(username='testing2', email='testing2@gmail.com', password='zaza1234')
        cls.profile2 = ProfileUserFactory(user = cls.user2)

        cls.lessontests = LessonTestsFactory(owner=cls.profile)
        cls.lessontests2 = LessonTestsFactory(owner=cls.profile2)

        TaskTestsFactory(lesson=cls.lessontests, position_in = 1, right_answer = 'd')
        TaskTestsFactory(lesson=cls.lessontests, position_in = 2, right_answer = 'd')
        TaskTestsFactory(lesson=cls.lessontests, position_in = 3, right_answer = 'd')
        # TaskTestsFactory.create_batch(5, lesson=cls.lessontests2)

        cls.form_data_correct = {'answers': '["d"]',
                        'lesson_id': 1,
                        'position_in':1,
                        'username':'testing2@gmail.com',
                        'result_content': []}
        cls.form_data_wrong = {'answers': ["a"],
                        'lesson_id': 1,
                        'position_in':2,
                        'username':'testing2@gmail.com',
                        'result_content': []}
        cls.form_data_full = {'answers': ["d"],
                        'lesson_id': 1,
                        'position_in':3,
                        'username':'testing2@gmail.com',
                        'result_content[]': [ '{"position":"1","result":"wrong"}',
                                            '{"position":"2","result":"correct"}',
                                            '{"position":"3","result":"active"}']
                        }
        cls.form_data_full_no_user = {'answers': ["d"],
                        'lesson_id': 1,
                        'position_in':3,
                        'username':'',
                        'result_content[]': [ '{"position":"1","result":"wrong"}',
                                            '{"position":"2","result":"correct"}',
                                            '{"position":"3","result":"active"}']
                        }


    def test_view_resolve(self):
        self.assertEqual(resolve(self.url).func.view_class, AjaxGetAnswerFromTask)

    def test_view_no_send_data(self):
        with self.assertRaises(AttributeError):
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 200)

    def test_view_with_data_correct(self):
        response = self.client.get(self.url, self.form_data_correct)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['status'], 'correct')

    def test_view_with_data_wrong(self):
        response = self.client.get(self.url, self.form_data_wrong)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['status'], 'wrong')

    def test_view_full_answer(self):
        response = self.client.get(self.url, self.form_data_full)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data['status'], 'correct')
        self.assertEqual(data['wrong'], 1)
        self.assertEqual(data['correct'], 2)

    def test_display_statistics_for_nonauthorized_users(self):
        response = self.client.get(self.url, self.form_data_full_no_user)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(data['statistic'], [])

    def test_display_statistics_for_authorized_users_one_data(self):
        response = self.client.get(self.url, self.form_data_full)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data['statistic'][0]['period']), 1)
        self.assertEqual(data['statistic'][0]['percentage'][0], 67)

    def test_display_statistics_for_authorized_users_three_data(self):
        form_data_full_1 = {'answers': ["d"],
                        'lesson_id': 1,
                        'position_in':3,
                        'username':'testing2@gmail.com',
                        'result_content[]': [ '{"position":"1","result":"correct"}',
                                            '{"position":"2","result":"correct"}',
                                            '{"position":"3","result":"correct"}']
                        }
        form_data_full_2 = {'answers': ["d"],
                        'lesson_id': 1,
                        'position_in':3,
                        'username':'testing2@gmail.com',
                        'result_content[]': [ '{"position":"1","result":"wrong"}',
                                            '{"position":"2","result":"wrong"}',
                                            '{"position":"3","result":"wrong"}']
                        }
        response = self.client.get(self.url, form_data_full_1)
        response = self.client.get(self.url, form_data_full_2)
        response = self.client.get(self.url, self.form_data_full)
        data = json.loads(response.content)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(data['statistic'][0]['period']), 3)
        self.assertEqual(data['statistic'][0]['percentage'], [100,0,67])