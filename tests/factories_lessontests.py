import uuid
import factory
from faker import Faker
from factory import fuzzy
import random

from .factories_common import BaseModelFactory
from .factories_users import ProfileUserFactory, UserFactory

from apps.lessontests.models import LessonTests, TaskTests, ResultTestModel


fake = Faker()


# help function for choices field
class ModelFieldLazyChoice(factory.LazyFunction):
    def __init__(self, model_class, field, *args, **kwargs):
        choices = [choice[1] for choice in model_class._meta.get_field(field).choices]
        super(ModelFieldLazyChoice, self).__init__(
            # function=lambda: random.choice(choices),
            function=lambda: choices[0],
            *args, **kwargs
        )


class LessonTestsFactory(BaseModelFactory):
    class Meta:
        model = LessonTests

    title = factory.Sequence(lambda n: 'Lesson test {}'.format(n)) 
    slug = factory.Sequence(lambda n: 'lesson-test-{}'.format(n))
    owner = factory.SubFactory(ProfileUserFactory)
    permission = ModelFieldLazyChoice(LessonTests, 'permission')

    # many to many field
    @factory.post_generation
    def permit(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for permit in extracted:
                self.permit.add(permit)


class TaskTestsFactory(BaseModelFactory):
    class Meta:
        model = TaskTests

    title = factory.Sequence(lambda n: 'Task {}'.format(n))
    slug = factory.Sequence(lambda n: 'task-{}'.format(n))
    lesson = factory.SubFactory(LessonTestsFactory)
    question = fake.text()
    a = factory.Sequence(lambda n: 'Answer A')
    b = factory.Sequence(lambda n: 'Answer B')
    c = factory.Sequence(lambda n: 'Answer C')
    d = factory.Sequence(lambda n: 'Answer D')
    right_answer = ModelFieldLazyChoice(TaskTests, 'right_answer')
    type_question = ModelFieldLazyChoice(TaskTests, 'type_question')
    position_in = factory.fuzzy.FuzzyInteger(0, 100)


class ResultTestModelFactory(BaseModelFactory):
    class Meta:
        model = ResultTestModel

    lesson = factory.SubFactory(LessonTestsFactory)
    user = factory.SubFactory(ProfileUserFactory)
    result = fake.text()