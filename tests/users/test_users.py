import sys
sys.path.append("..")

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.urls import reverse
from django.test import TestCase, Client

from tests.factories_users import UserFactory, ProfileUserFactory
from apps.users.forms import CustomAuthenticationForm, CustomUserCreationForm

User = get_user_model()


class CreateUpdateDeleteUserTestCase(TestCase):

    def setUp(self):
        self.user = User.objects._create_user(email = 'asd@gmail.com', username='asd', password='12121212')
        self.user.set_password('121212')
        self.user.save()

    def test_create_user_invalid_active(self):
        self.assertFalse(self.user.is_active)

    def test_create_user_valid_active(self):
        self.user.is_active = True
        self.user.save()
        self.assertTrue(self.user.is_active)

    def test_new_user_email_normalized(self):
        email = 'test@GMAIL.COM'
        user = get_user_model().objects._create_user(email, '12121212')
        self.assertEqual(user.email, email.lower())

    def test_new_user_invalid_email(self):
        ''' creating user with no email raises error '''
        with self.assertRaises(ValueError):
            get_user_model().objects._create_user(None, '12121212')

    def test_create_new_superuser(self):
        user = get_user_model().objects.create_superuser('test@gmail.com', 'zaza1234')
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)

class CreateUpdateDeleteUserTestCase(TestCase):

    def setUp(self):
        self.user = User.objects._create_user(email = 'asd@gmail.com', username='asd', password='12121212')
        self.user.set_password('121212')
        self.user.is_active = True
        self.user.save()


    def test_create_profile_without_select_choices(self):
        profile = ProfileUserFactory(user = self.user)
        self.assertEqual(profile.user, self.user)
        self.assertEqual(profile.user.email, 'asd@gmail.com')
        self.assertEqual(profile.person, 'Teacher')

    def test_create_profile_student(self):
        profile = ProfileUserFactory(user = self.user, person = 'Student')
        self.assertEqual(profile.user, self.user)
        self.assertEqual(profile.user.email, 'asd@gmail.com')
        self.assertEqual(profile.person, 'Student')




class UsersTestCase(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.client = Client()
        cls.user = UserFactory()
        cls.url = reverse('users:login')
        cls.logout = reverse('users:logout')

    def test_current_user_is_anonymous(self):
        response = self.client.get(self.url)
        curr_user = response.context["user"]
        self.assertEqual(curr_user, AnonymousUser())

    def test_login_url_accessible_by_name(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_login_user(self):
        self.user.is_active = True
        self.user.save()
        self.client.force_login(self.user)
        response = self.client.get('/')
        curr_user = response.wsgi_request.user
        self.assertEqual(curr_user, self.user)

    def test_logout_user(self):
        self.client.force_login(self.user)
        response = self.client.get(self.logout)
        self.assertEqual(response.status_code, 302)


class UserAuthenticationFormTestCase(TestCase):

    def setUp(self):
        self.user = UserFactory(username='testing', email='testing@gmail.com', password=12121212)
        self.form = CustomAuthenticationForm

    def test_form_invalid_password(self):
        form = self.form(data={'username': 'testing@gmail.com', 'password':112223344})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 1)
        self.assertEquals(form.errors['password'][0], 'Неправильный пароль.')

    def test_form_invalid_password_both_fields(self):
        form = self.form(data={'username': 'testing@testing.com', 'password':112223344})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['username'][0], 'Пользователь с таким е-мейлов не существует.')
        self.assertEquals(form.errors['password'][0], 'Неправильный пароль.')



class UserCreationFormTestCase(TestCase):

    def setUp(self):
        self.user = UserFactory(username='testing', email='testing@gmail.com')
        self.form = CustomUserCreationForm

    def test_form_is_valid_full_info(self):
        CHOICES = (
            ('T', 'Teacher'),
            ('S', 'Student'),
        )
        form = self.form(data = {'username':'test', 'email':'test@test.com', 'password1':'123123123', 'password2':'123123123', 'select':CHOICES[0][0]})
        self.assertTrue(form.is_valid())

    def test_form_is_invalid_no_username(self):
        form = self.form(data = {'email':'test@test.com', 'password1':'123123123', 'password2':'123123123'})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['username'][0], 'Обязательное поле.')

    def test_form_invalid_username(self):
        form = self.form(data = {'email':'test@test.com', 'password1':'123123123', 'password2':'123123123', 'username':'testing'})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['username'][0], 'Пользователь с таким логином уже существует.')

    def test_form_differend_passwords(self):
        form = self.form(data={'email': 'test@test.com', 'password1': '123123123', 'password2': '12121212', 'username': 'test'})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['password2'][0], 'Пароли не совпадают.')

    def test_form_invalid_email(self):
        form = self.form(data = {'email':'testing@gmail.com', 'password1':'123123123', 'password2':'123123123', 'username':'test'})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['email'][0], 'Пользователь с таким е-мейлов уже существует.')

    def test_form_invalid_email_forget_sign(self):
        form = self.form(data = {'email':'testing#test.com', 'password1':'123123123', 'password2':'123123123', 'username':'test'})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['email'][0], 'Пропустили знак @ в адресе электронной почты')

    def test_form_invalid_email_forget_dot(self):
        form = self.form(data = {'email':'testing@test,com', 'password1':'123123123', 'password2':'123123123', 'username':'test'})
        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 2)
        self.assertEquals(form.errors['email'][0], 'Пропустили знак . в адресе электронной почты')

