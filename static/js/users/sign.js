
var DOMstrings = {
    inputName: 'id_username',
    inputEmail: 'id_email',
    inputPassword: 'id_password1',
    inputPasswordConfirm: 'id_password2',
    selectVal: 'id_select',
    signUpBtn: '.signup__btn',
    errorList: '.error-list',
    iconClass: '.fa'
};


var username, email, pass, passConfirm, selectVal, selectChoice, nxtElement, passVal, passConfVal, allData, viewUrl;
email = document.getElementById(DOMstrings.inputEmail).value;



_.each(["keyup", "keydown", "change"], function(event){
    document.activeElement.addEventListener(event, getCurrentFieldError);
});

function getCurrentFieldError() {
    if(document.activeElement.id == 'id_email') {
        validateEmail();
    } else if(document.activeElement.id == 'id_password1') {
        validatePasswordFirst();
        checkBothPassword();
    } else if(document.activeElement.id == 'id_password2') {
        validatePasswordFirst();
        checkBothPassword();
    } else {
        emptyErrorToggle();
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//                        ADD ICONS AND DISLAY PASSWORD LETTERS
///////////////////////////////////////////////////////////////////////////////////////////////////

document.getElementById(DOMstrings.inputPassword).insertAdjacentHTML('beforebegin', '<i class="fa fa-eye active"></i>');
document.getElementById(DOMstrings.inputPasswordConfirm).insertAdjacentHTML('beforebegin', '<i class="fa fa-eye active"></i>');


document.getElementById(DOMstrings.inputPassword).parentNode.querySelector(DOMstrings.iconClass).addEventListener('click', iconToggle);
document.getElementById(DOMstrings.inputPasswordConfirm).parentNode.querySelector(DOMstrings.iconClass).addEventListener('click', iconToggle);

function iconToggle(event) {
    var faClsName;
    
    faClsName = event.target.className;
    if (faClsName.includes('active')) {
        event.target.classList.remove('active');
        event.target.classList.remove('fa-eye');
        event.target.classList.add('fa-eye-slash');
        event.target.nextSibling.type='text';
    } else {
        event.target.classList.add('active');
        event.target.classList.add('fa-eye');
        event.target.classList.remove('fa-eye-slash'); 
        event.target.nextSibling.type='password';       
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//                        REMOVE EMPTY FIELD AND ADD ERROR IF EMPTY
///////////////////////////////////////////////////////////////////////////////////////////////////

function emptyErrorToggle(){   
    if(document.activeElement.value == '') {
        nxtElement = document.activeElement.nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.activeElement.nextElementSibling.remove();
        }
        document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Поле не может быть пустым!!!</div>');
    } else {
        nxtElement = document.activeElement.nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.activeElement.nextElementSibling.remove();
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//                        VALIDATE PASSWORD FIELD
///////////////////////////////////////////////////////////////////////////////////////////////////

function validatePasswordFirst(){
    if(document.activeElement.value == '') {
        nxtElement = document.activeElement.nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.activeElement.nextElementSibling.remove();
        }
        document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Поле не может быть пустым!!!</div>');
    } else if(document.activeElement.value.length < 8) {
        nxtElement = document.activeElement.nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.activeElement.nextElementSibling.remove();
        }
        document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Пароль должен состоять не менее чем из 8 символов!!!</div>');
    } else {
        nxtElement = document.activeElement.nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.activeElement.nextElementSibling.remove();
        }
    }
}


function checkBothPassword() {
    passVal = document.getElementById(DOMstrings.inputPassword).value;
    passConfVal = document.getElementById(DOMstrings.inputPasswordConfirm).value;

    if( (passVal.length >=8) && (passConfVal.length >= 8) && (passVal != passConfVal) ) {
        nxtElement = document.getElementById(DOMstrings.inputPassword).nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.getElementById(DOMstrings.inputPassword).nextElementSibling.nextElementSibling.remove();
        }
        document.getElementById(DOMstrings.inputPassword).insertAdjacentHTML('afterend','<div class="error-list">Пароли не совпадают!!!</div>');
    }
}



///////////////////////////////////////////////////////////////////////////////////////////////////
//                           EMAIL VALIDATION
///////////////////////////////////////////////////////////////////////////////////////////////////
function validateEmail(){
    // NotFoundError: Node was not found - когда была ошибка отсутствие данных и начинаем вводить новые данные
    nxtElement = document.activeElement.nextElementSibling;
    if(nxtElement && nxtElement.classList[0] == 'error-list') {
        document.activeElement.nextElementSibling.remove();
    }
    
    if(!isEmail(document.activeElement.value) && document.activeElement.value != '') {
        document.getElementById(DOMstrings.inputEmail).insertAdjacentHTML('afterend',`
            <div class="error-list">Невалидный емайл!!!</div>
        `)
    } else if(document.activeElement.value == '') {
        document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Поле не может быть пустым!!!</div>');
    }
}

var isEmail = function(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
};


///////////////////////////////////////////////////////////////////////////////////////////////////
//                           GET DATA    ----     DJANGO VIEW
///////////////////////////////////////////////////////////////////////////////////////////////////


document.querySelector(DOMstrings.signUpBtn).addEventListener('click', validateFields)

function validateFields(event) {

    event.preventDefault();

    username = {'selector':DOMstrings.inputName, 'value': document.getElementById(DOMstrings.inputName).value};
    email = {'selector':DOMstrings.inputEmail, 'value': document.getElementById(DOMstrings.inputEmail).value};
    pass = {'selector':DOMstrings.inputPassword, 'value': document.getElementById(DOMstrings.inputPassword).value};
    passConfirm = {'selector':DOMstrings.inputPasswordConfirm, 'value': document.getElementById(DOMstrings.inputPasswordConfirm).value};

    selectChoice = document.getElementById(DOMstrings.selectVal);
    selectVal = selectChoice.options[selectChoice.selectedIndex].text;
    


    // EMPTY VALUES
    _.each([username, email, pass, passConfirm], function(item){

        // NotFoundError: Node was not found - когда была ошибка отсутствие данных и начинаем вводить новые данные
        nxtElement = document.getElementById(item.selector).nextElementSibling;
        if(nxtElement && nxtElement.classList[0] == 'error-list') {
            document.getElementById(item.selector).nextElementSibling.remove();
        }

        if(item.value == '') {
            document.getElementById(item.selector).insertAdjacentHTML('afterend',`
                <div class="error-list">Поле не может быть пустым!!!</div>
            `)
        }
    });

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    //                           SEND DATA  TO DJANGO 
    ///////////////////////////////////////////////////////////////////////////////////////////////////

    // if we have no error-list   --->   send data 
    if(document.querySelectorAll(DOMstrings.errorList).length == 0) {

        function getCookie(name) {
		    var cookieValue = null;
		    if (document.cookie && document.cookie !== '') {
		        var cookies = document.cookie.split(';');
		        for (var i = 0; i < cookies.length; i++) {
		            var cookie = jQuery.trim(cookies[i]);
		            // Does this cookie string begin with the name we want?
		            if (cookie.substring(0, name.length + 1) === (name + '=')) {
		                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
		                break;
		            }
		        }
		    }
		    return cookieValue;
		}
        var csrftoken = getCookie('csrftoken');
        
        viewUrl = document.querySelector(DOMstrings.signUpBtn).getAttribute('data-id');


        allData = {
            'username': username.value,
            'email': email.value,
            'password': pass.value,
            'person': selectVal,
            csrfmiddlewaretoken: csrftoken
        };
        
        $.ajax({
            type:'POST',
            url: viewUrl,
            dataType: 'json',
            data: allData,
            success: function(data){
                console.log(data)
                if (data.success) {
                    
                    //    MODAL  ACTION
                    var modal = document.getElementById("myModal");
                    var span = document.getElementsByClassName("close")[0];
                    modal.style.display = "block";
                    span.onclick = function() {
                        modal.style.display = "none";
                    };
                    window.onclick = function(event) {
                        if (event.target == modal) {
                            modal.style.display = "none";
                        }
                    };

                    // REDIRECT TO HOME PAGE
                    var myVar = setInterval(myTimer, 1000);
                    function myTimer() {
                        var newSec = parseInt(document.getElementById("modal-timer").innerHTML) - 1;
                        document.getElementById("modal-timer").innerHTML = newSec;
                    }
                    setTimeout(function(){
                        window.location.href = "/";
                    }, 5000);
                } else {

                    document.querySelector(DOMstrings.errorList)
                        ? document.querySelector(DOMstrings.errorList).remove() : null
                    _.each(data.errors, function(item){
                        document.getElementById(item.selector).insertAdjacentHTML('afterend',`<div class="error-list">${item.value}</div>`);
                    });
                }   
            }
        });
    }
}