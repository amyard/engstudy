var DOMstrings = {
    sidebarBtn: '.general_section-sidebar--counter',
    sidebarBtnActive: '.general_section-sidebar--counter.active',
    questionSection: '.general_section-content-item',
    questionSectionActive: '.general_section-content-item.active',
    sendBtn: 'btn-send',
    checkboxes: 'question-checkboxes--input',
    checkboxGeneral: 'question-checkboxes',
    taskQuestion: 'task-question',
    lesson: 'general_section-content',
    emptyClass: 'empty',
    userName: '.profile_btn',
    modal: 'modal-general',
    notAllowed: '.not-allowed'
}

///////////////////////////////////////////////////////////////////////////////////////////////////
//                           нажимаем на номер задания слева
///////////////////////////////////////////////////////////////////////////////////////////////////
function clickSidebarBtn(event) {    
    var clsName = event.target.parentNode.classList,
        dataId = event.target.parentNode.getAttribute('data-id');
    
    if(dataId === null) {
        var clsName = event.target.classList,
            dataId = event.target.getAttribute('data-id');
    }

    var questionId = 'question-'+dataId;

    document.querySelector(DOMstrings.sidebarBtnActive).classList.remove('active');
    clsName.add('active');
    document.querySelector(DOMstrings.questionSectionActive).classList.remove('active');
    document.getElementById(questionId).classList.add('active');
    document.getElementById(questionId).getElementsByClassName(DOMstrings.taskQuestion)[0].style.cssText = 'color: black';
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//                           add checked to checkbox
///////////////////////////////////////////////////////////////////////////////////////////////////

function addCheckedToCheckbox(event) {
    event.target.getAttribute('checked')
        ? event.target.removeAttribute('checked')
        : event.target.setAttribute('checked', true)
}


///////////////////////////////////////////////////////////////////////////////////////////////////
//       если вернулись на вопрос, который отвеченный, то не даем повторно ответить
///////////////////////////////////////////////////////////////////////////////////////////////////

btnConfirm = document.getElementById(DOMstrings.sendBtn);



btnConfirm.addEventListener("mouseover", event => {
    event.target.classList.add('not-allowed');
});

btnConfirm.addEventListener("mouseout", event => {
    event.target.classList.remove('not-allowed');
});

///////////////////////////////////////////////////////////////////////////////////////////////////
//                           отправить ответ
///////////////////////////////////////////////////////////////////////////////////////////////////

function clickConfirmAnswer(event){
    var btnId = parseInt(event.target.getAttribute('data-id').replace('confirm_btn-', '')),
        questionDiv = 'question-'+btnId;
        allCheckboxes = document.getElementById(questionDiv).getElementsByClassName(DOMstrings.checkboxes),
        taskTitle = document.getElementById(questionDiv).getElementsByClassName(DOMstrings.taskQuestion),
        answers = [],
        urlHref = event.target.getAttribute('data-href'),
        lessonId = document.getElementsByClassName(DOMstrings.lesson)[0].getAttribute('data-id'),
        countIncompleteTasks = document.getElementsByClassName(DOMstrings.emptyClass).length-1,
        resultContent = [],
        allResults = [],
        userName = '';


    _.each(allCheckboxes, function(item){
        if(item.getAttribute('checked')){
            answers.push(item.getAttribute('id').replace('qs-', ''))
        }
    });

    // если вопрос последний , то делаем это
    if(countIncompleteTasks == 0) {
        _.each( document.querySelectorAll(DOMstrings.sidebarBtn) , function(value, item){
            var clsResult = value.classList.value.replace('general_section-sidebar--counter sidebar-btn', '').replace('empty', '').replace(' ',''),
                clsPosition = value.getAttribute('data-id');
            allResults.push(JSON.stringify({'position': clsPosition, 'result': clsResult}))
        });

        userName = document.querySelector(DOMstrings.userName) != null ? document.querySelector(DOMstrings.userName).getAttribute('data-id') : 'anonym'
    }
    
    
    // если не выбрали ответ и нажали отправить
    if (document.querySelector(DOMstrings.notAllowed)) {
        document.querySelector(DOMstrings.notAllowed).addEventListener('click', event => {
            event.preventDefault();
        });
    } else if(answers.length == 0) {
        taskTitle[0].style.cssText = 'color: red';
    } else {
        data = {
            'answers': JSON.stringify(answers),
            'position_in': btnId,
            'lesson_id': lessonId,
            'result_content': allResults,
            'username': userName,
        };

        $.ajax({
            type:'GET',
            url: urlHref,
            dataType: 'json',
            data: data,
            success: function(data){
                document.querySelector(DOMstrings.sidebarBtnActive).classList.add(data.status);
                document.querySelector(DOMstrings.sidebarBtnActive).classList.remove('empty');
                document.querySelector(DOMstrings.sidebarBtnActive).classList.remove('active');
                nextQuestionId = 'question-' + (btnId + 1);
                oldQuestionId = 'question-' + btnId;

                document.getElementById(oldQuestionId).classList.remove('active');


                // первый if - берем следующий елемент после ответа
                // второе условие - находим любой не отвеченный вопрос
                // else - все тесты пройденны. оставляем блок последний видный. не скрываем его
                if (document.getElementById(nextQuestionId) != null && document.querySelectorAll(DOMstrings.sidebarBtn)[btnId].classList.contains('empty')) {
                    document.getElementById(nextQuestionId).classList.add('active');     
                    document.querySelectorAll(DOMstrings.sidebarBtn)[btnId].classList.add('active');
                } else if ( document.getElementsByClassName(DOMstrings.emptyClass)[0] != null ) {
                    newId = document.getElementsByClassName(DOMstrings.emptyClass)[0].getAttribute('data-id')
                    nextQuestionId = 'question-' + newId;
                    document.getElementById(nextQuestionId).classList.add('active');     
                    document.querySelectorAll(DOMstrings.sidebarBtn)[newId-1].classList.add('active');
                } else { 
                    document.getElementById(oldQuestionId).classList.add('active');
                    console.log(' It was last question. ')


                    // PIE CHART 
                    percent = Math.round((data.correct / (data.correct + data.wrong))*100, 1)
                    document.querySelector('.modal-content').insertAdjacentHTML('afterbegin',`
                        <div class="container"> \
                            <h3 style="text-align: center;">${data.your_result} ${percent}%.</h3> \
                            <div class="first-chart"> \
                                <canvas id="myChart"></canvas> \
                            </div> \
                        </div> \
                    `)
                    $("#modal-general").modal("show");


                    var ctx = document.getElementById("myChart").getContext('2d');
                    var myChart = new Chart(ctx, {
                        type: 'doughnut',
                        data: {
                            labels: [data.corr_res, data.wrong_res],
                            datasets: [{
                                backgroundColor: ["#2ecc71", "#e74c3c"],
                                data: [data.correct, data.wrong]
                            }]
                        }
                    });
                    

                    // DISPLAY GISTORY RESULT
                    console.log(data.history_result)
                    console.log(data.perc_correct)
                    if(data.statistic.length == 1) {
                        document.querySelector('.first-chart').insertAdjacentHTML('afterend',`
                            <h3 style="text-align: center; margin-top: 2rem;">${data.history_result}</h3> \
                            <div> \
                                <canvas id="secondChart"></canvas> \
                            </div> \
                        `);

                        function addPercentage(value) {
                            return value+'%';
                        }
                        var ctx2 = document.getElementById("secondChart").getContext('2d');
        
                        var lineChart = new Chart(ctx2, {
                            type: 'line',
                            data: {
                                labels: data.statistic[0].period,
                                datasets: [{
                                    label: data.perc_correct,
                                    borderColor: 'rgba(75, 192, 192, 1)',
                                    backgroundColor: 'rgba(75, 192, 192, 0.2)',
                                    data: data.statistic[0].percentage
                                }]
                            },
                            options: {            
                                scales: {
                                    yAxes: [{
                                        ticks: {
                                            beginAtZero: true,
                                            callback: function(value, index, values) {
                                                return addPercentage(value);
                                            },
                                            max: 110,
                                            min: 0,
                                            stepSize: 10,
                                        }
                                    }]                
                                }
                            },
                        });
                    }                                   
                }
            }
        })
    }
}
