var DOMstrings = {
    title: '.field-title',
    slug: '.field-slug',
    selectVal: 'id_type_question',
    ans_a: '.field-a',
    ans_b: '.field-b',
    ans_c: '.field-c',
    ans_d: '.field-d',
    right_ans: '.field-right_answer',
    custom_right_ans: '.field-custom_right_answer',
    audio_file: '.field-audio_file',
};

// дефолтная загрузка 
selectChoice = document.getElementById(DOMstrings.selectVal);
selectVal = selectChoice.options[selectChoice.selectedIndex].value;

blockAction(document.querySelector(DOMstrings.title).parentNode, 'none');

toggleField(selectVal);



// меняем значения в дропдауне
document.getElementById(DOMstrings.selectVal).addEventListener('change', function(){
    toggleField(event.target.value);
});


function toggleField(value) {
    if (value == 'choose') {
        blockAction(document.querySelector(DOMstrings.ans_a), 'block');
        blockAction(document.querySelector(DOMstrings.ans_b), 'block');
        blockAction(document.querySelector(DOMstrings.ans_c), 'block');
        blockAction(document.querySelector(DOMstrings.ans_d), 'block');
        blockAction(document.querySelector(DOMstrings.right_ans), 'block');
        blockAction(document.querySelector(DOMstrings.custom_right_ans), 'none');
        blockAction(document.querySelector(DOMstrings.audio_file), 'none');
    } else if (value == 'type' || value == 'move' || value == 'collect') {
        blockAction(document.querySelector(DOMstrings.ans_a), 'none');
        blockAction(document.querySelector(DOMstrings.ans_b), 'none');
        blockAction(document.querySelector(DOMstrings.ans_c), 'none');
        blockAction(document.querySelector(DOMstrings.ans_d), 'none');
        blockAction(document.querySelector(DOMstrings.right_ans), 'none');
        blockAction(document.querySelector(DOMstrings.custom_right_ans), 'block');
        blockAction(document.querySelector(DOMstrings.audio_file), 'none');
    } else if (value == 'audio') {
        blockAction(document.querySelector(DOMstrings.ans_a), 'none');
        blockAction(document.querySelector(DOMstrings.ans_b), 'none');
        blockAction(document.querySelector(DOMstrings.ans_c), 'none');
        blockAction(document.querySelector(DOMstrings.ans_d), 'none');
        blockAction(document.querySelector(DOMstrings.right_ans), 'none');
        blockAction(document.querySelector(DOMstrings.custom_right_ans), 'none');
        blockAction(document.querySelector(DOMstrings.audio_file), 'block');
    }
};


function blockAction(selector, action) {
    selector.style.display = action;
}