import json
import datetime

res = [(1, "[{'position': '1', 'result': 'correct'}, {'position': '2', 'result': 'wrong'}, {'position': '3', 'result': 'wrong'}, {'position': '4', 'result': 'correct'}]"), (15, "[{'position': '1', 'result': 'correct'}, {'position': '2', 'result': 'correct'}, {'position': '3', 'result': 'wrong'}, {'position': '4', 'result': 'correct'}]"), (16, "[{'position': '1', 'result': 'wrong'}, {'position': '2', 'result': 'wrong'}, {'position': '3', 'result': 'wrong'}, {'position': '4', 'result': 'wrong'}]"), (17, "[{'position': '1', 'result': 'correct'}, {'position': '2', 'result': 'wrong'}, {'position': '3', 'result': 'wrong'}, {'position': '4', 'result': 'wrong'}]")]

for i in res:
    wrong = i[1].count('wrong')
    correct = i[1].count('correct')
    res = round((correct / (correct+wrong)) *100)
    print(res if correct != 0 else 0)


res = datetime.datetime(2019, 11, 17, 23, 12, 42, 21894)
res = res.strftime('%d/%m/%Y')

print(res)