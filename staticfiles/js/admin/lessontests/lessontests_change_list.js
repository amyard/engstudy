var DOMstrings = {
    title: '.field-title',
    slug: '.field-slug',
    selectVal: 'id_type_question',
    ans_a: '.field-a',
    ans_b: '.field-b',
    ans_c: '.field-c',
    ans_d: '.field-d',
    right_ans: '.field-right_answer',
    custom_right_ans: '.field-custom_right_answer',
    audio_file: '.field-audio_file',
    
    permission: 'id_permission',
    permit: '.field-permit',
    typeQuestion: '.field-type_question',
    adminTestTask: 'has_original',
    addNewTaskBtn: 'add-row',
};

//  отображение поля для разрешения
blockAction(document.querySelector(DOMstrings.slug), 'none');
selectChoice = document.getElementById(DOMstrings.permission);
selectVal = selectChoice.options[selectChoice.selectedIndex].value;

toggleFieldPermission(selectVal);

// меняем значения в дропдауне в permission
document.getElementById(DOMstrings.permission).addEventListener('change', function(){
    toggleFieldPermission(event.target.value);
});

function toggleFieldPermission(value) {
    value == 'A'
        ? blockAction(document.querySelector(DOMstrings.permit), 'none')
        : blockAction(document.querySelector(DOMstrings.permit), 'block')
};




// TODO - закончить загрузку. нужно итерировать по всем значениям
var allData = document.getElementsByClassName(DOMstrings.adminTestTask);

[...allData].forEach(function(element){
    selectVal = element.getElementsByTagName('select')[0].value;
    toggleField(element, selectVal);
});






// меняем значения в дропдауне в тасках
document.querySelector(DOMstrings.typeQuestion).getElementsByTagName('select')[0].addEventListener('change', function(){
    toggleField(event.target, event.target.value);
});


// добавить новое поле с таской
// console.log(  document.querySelector('.add-row').getElementsByTagName('a')[0]  )


setTimeout(function(){
    document.querySelector('.add-row').addEventListener('click', testScript)
}, 100)



function testScript() {

    // need some time to display all data. fucking js.
    setTimeout(function(){
        var allData = document.getElementsByClassName(DOMstrings.adminTestTask);

        [...allData].forEach(function(element){
            selectVal = element.getElementsByTagName('select')[0].value;
            console.log(element)
            console.log(selectVal)
            toggleField(element, selectVal);
        });
    }, 100);
};


function toggleField(position, value) {

    var ids = position.getAttribute('id');

    typeof document.getElementById(ids).getElementsByTagName('fieldset')[0] !== 'undefined'
        ? selector = document.getElementById(ids).getElementsByTagName('fieldset')[0]
        : selector = document.getElementById(ids).closest('fieldset')

    if (value == 'choose') {
        blockAction(selector.querySelector(DOMstrings.ans_a), 'block');
        blockAction(selector.querySelector(DOMstrings.ans_b), 'block');
        blockAction(selector.querySelector(DOMstrings.ans_c), 'block');
        blockAction(selector.querySelector(DOMstrings.ans_d), 'block');
        blockAction(selector.querySelector(DOMstrings.right_ans), 'block');
        blockAction(selector.querySelector(DOMstrings.custom_right_ans), 'none');
        blockAction(selector.querySelector(DOMstrings.audio_file), 'none');
    } else if (value == 'type' || value == 'move' || value == 'collect') {
        blockAction(selector.querySelector(DOMstrings.ans_a), 'none');
        blockAction(selector.querySelector(DOMstrings.ans_b), 'none');
        blockAction(selector.querySelector(DOMstrings.ans_c), 'none');
        blockAction(selector.querySelector(DOMstrings.ans_d), 'none');
        blockAction(selector.querySelector(DOMstrings.right_ans), 'none');
        blockAction(selector.querySelector(DOMstrings.custom_right_ans), 'block');
        blockAction(selector.querySelector(DOMstrings.audio_file), 'none');
    } else if (value == 'audio') {
        blockAction(selector.querySelector(DOMstrings.ans_a), 'none');
        blockAction(selector.querySelector(DOMstrings.ans_b), 'none');
        blockAction(selector.querySelector(DOMstrings.ans_c), 'none');
        blockAction(selector.querySelector(DOMstrings.ans_d), 'none');
        blockAction(selector.querySelector(DOMstrings.right_ans), 'none');
        blockAction(selector.querySelector(DOMstrings.custom_right_ans), 'none');
        blockAction(selector.querySelector(DOMstrings.audio_file), 'block');
    }
};


function blockAction(selector, action) {
    selector.style.display = action;
}