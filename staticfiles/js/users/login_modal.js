// CLICK ON LOGIN BTN TO DISPLAY MODAL
$(document).on('click', '.signin_modal', function (event) {
    event.preventDefault();

    $.ajax({
        url: $(this).attr('href'),
        type: 'get',
        dataType: 'json',
        beforeSend: function () {
            $("#modal-general").modal("show");
        },
        success: function (data) {
            $("#modal-general .modal-content").html(data.html_form);

            // GENERAL ACTIONS
            var DOMstrings = {
                inputEmail: 'id_username',
                inputPassword: 'id_password',
                signUpBtn: '.signup__btn',
                errorList: '.error-list',
                iconClass: '.fa',
                headerDiv: '.navbar',
                alert: 'alert'
            };


            var username, pass, nxtElement, passVal, passConfVal, allData, viewUrl;


            _.each(["keyup", "keydown", "change"], function(event){
                document.activeElement.addEventListener(event, getCurrentFieldError);
            });

            function getCurrentFieldError() {
                if(document.activeElement.id == 'id_password') {
                    validatePasswordFirst();
                } else if(document.activeElement.id == 'id_username') {
                    validateEmail();
                } else {
                    emptyErrorToggle();
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////////
            //                        ADD ICONS AND DISLAY PASSWORD LETTERS
            ///////////////////////////////////////////////////////////////////////////////////////////////////

            document.getElementById(DOMstrings.inputPassword).insertAdjacentHTML('beforebegin', '<i class="fa fa-eye active"></i>');
            document.getElementById(DOMstrings.inputPassword).parentNode.querySelector(DOMstrings.iconClass).addEventListener('click', iconToggle);

            function iconToggle(event) {
                var faClsName;
                
                faClsName = event.target.className;
                if (faClsName.includes('active')) {
                    event.target.classList.remove('active');
                    event.target.classList.remove('fa-eye');
                    event.target.classList.add('fa-eye-slash');
                    event.target.nextSibling.type='text';
                } else {
                    event.target.classList.add('active');
                    event.target.classList.add('fa-eye');
                    event.target.classList.remove('fa-eye-slash'); 
                    event.target.nextSibling.type='password';       
                }
            }


            ///////////////////////////////////////////////////////////////////////////////////////////////////
            //                        REMOVE EMPTY FIELD AND ADD ERROR IF EMPTY
            ///////////////////////////////////////////////////////////////////////////////////////////////////

            function emptyErrorToggle(){   
                if(document.activeElement.value == '') {
                    nxtElement = document.activeElement.nextElementSibling;
                    if(nxtElement && nxtElement.classList[0] == 'error-list') {
                        document.activeElement.nextElementSibling.remove();
                    }
                    document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Поле не может быть пустым!!!</div>');
                } else {
                    nxtElement = document.activeElement.nextElementSibling;
                    if(nxtElement && nxtElement.classList[0] == 'error-list') {
                        document.activeElement.nextElementSibling.remove();
                    }
                }
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////////
            //                        VALIDATE PASSWORD FIELD
            ///////////////////////////////////////////////////////////////////////////////////////////////////

            function validatePasswordFirst(){

                if(document.activeElement.value == '') {
                    nxtElement = document.activeElement.nextElementSibling;
                    if(nxtElement && nxtElement.classList[0] == 'error-list') {
                        document.activeElement.nextElementSibling.remove();
                    }
                    document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Поле не может быть пустым!!!</div>');
                } else if(document.activeElement.value.length < 8) {
                    nxtElement = document.activeElement.nextElementSibling;
                    if(nxtElement && nxtElement.classList[0] == 'error-list') {
                        document.activeElement.nextElementSibling.remove();
                    }
                    document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Пароль должен состоять не менее чем из 8 символов!!!</div>');
                } else {
                    nxtElement = document.activeElement.nextElementSibling;
                    if(nxtElement && nxtElement.classList[0] == 'error-list') {
                        document.activeElement.nextElementSibling.remove();
                    }
                }
            }



            ///////////////////////////////////////////////////////////////////////////////////////////////////
            //                           EMAIL VALIDATION
            ///////////////////////////////////////////////////////////////////////////////////////////////////
            function validateEmail(){
                // NotFoundError: Node was not found - когда была ошибка отсутствие данных и начинаем вводить новые данные
                nxtElement = document.activeElement.nextElementSibling;
                if(nxtElement && nxtElement.classList[0] == 'error-list') {
                    document.activeElement.nextElementSibling.remove();
                }
                
                if(!isEmail(document.activeElement.value) && document.activeElement.value != '') {
                    document.getElementById(DOMstrings.inputEmail).insertAdjacentHTML('afterend',`
                        <div class="error-list">Невалидный емайл!!!</div>
                    `)
                } else if(document.activeElement.value == '') {
                    document.activeElement.insertAdjacentHTML('afterend','<div class="error-list">Поле не может быть пустым!!!</div>');
                }
            }

            var isEmail = function(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            };


            ///////////////////////////////////////////////////////////////////////////////////////////////////
            //                           GET DATA    ----     DJANGO VIEW
            ///////////////////////////////////////////////////////////////////////////////////////////////////


            document.querySelector(DOMstrings.signUpBtn).addEventListener('click', validateFields)

            function validateFields(event) {

                event.preventDefault();

                username = {'selector':DOMstrings.inputEmail, 'value': document.getElementById(DOMstrings.inputEmail).value};
                pass = {'selector':DOMstrings.inputPassword, 'value': document.getElementById(DOMstrings.inputPassword).value};


                // EMPTY VALUES
                _.each([username, pass], function(item){

                    // NotFoundError: Node was not found - когда была ошибка отсутствие данных и начинаем вводить новые данные
                    nxtElement = document.getElementById(item.selector).nextElementSibling;
                    if(nxtElement && nxtElement.classList[0] == 'error-list') {
                        document.getElementById(item.selector).nextElementSibling.remove();
                    }

                    if(item.value == '') {
                        document.getElementById(item.selector).insertAdjacentHTML('afterend',`
                            <div class="error-list">Поле не может быть пустым!!!</div>
                        `)
                    }
                });

                ///////////////////////////////////////////////////////////////////////////////////////////////////
                //                           SEND DATA  TO DJANGO 
                ///////////////////////////////////////////////////////////////////////////////////////////////////

                // if we have no error-list   --->   send data 
                if(document.querySelectorAll(DOMstrings.errorList).length == 0) {

                    function getCookie(name) {
                        var cookieValue = null;
                        if (document.cookie && document.cookie !== '') {
                            var cookies = document.cookie.split(';');
                            for (var i = 0; i < cookies.length; i++) {
                                var cookie = jQuery.trim(cookies[i]);
                                // Does this cookie string begin with the name we want?
                                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                                    break;
                                }
                            }
                        }
                        return cookieValue;
                    }
                    var csrftoken = getCookie('csrftoken');
                    
                    viewUrl = document.querySelector(DOMstrings.signUpBtn).getAttribute('data-id');


                    allData = {
                        'username': username.value,
                        'password': pass.value,
                        csrfmiddlewaretoken: csrftoken
                    };
                    
                    $.ajax({
                        type:'POST',
                        url: viewUrl,
                        dataType: 'json',
                        data: allData,
                        success: function(data){
                            // TODO - заменять кнопки регистрации на другие 
                            if (data.success) {    
                                $("#modal-general").modal("hide");
                                [...document.getElementsByClassName(DOMstrings.alert)].map(n => n && n.remove());
                                document.querySelector(DOMstrings.headerDiv).insertAdjacentHTML('afterend',`
                                    <div class="alert alert-success"><b>${data.success_message}</b></div>
                                `);

                                document.querySelector('.reg_btn').innerHTML = 'Profile';
                                document.querySelector('.reg_btn').setAttribute('href', '#');
                                document.querySelector('.reg_btn').classList.replace('reg_btn', 'profile_btn')

                                document.querySelector('.signin_modal').innerHTML = 'Log Out';
                                document.querySelector('.signin_modal').setAttribute('href', '/users/logout/');
                                document.querySelector('.signin_modal').classList.replace('signin_modal', 'logout_btn')


                                $(".alert").fadeTo(3000, 500).slideUp(500, function(){
                                    $(".alert").slideUp(500);
                                });                             
                            } else {
                                document.querySelector(DOMstrings.errorList)
                                    ? document.querySelector(DOMstrings.errorList).remove() 
                                    : null
                                _.each(data.errors, function(item){
                                    document.getElementById(item.selector).insertAdjacentHTML('afterend',`<div class="error-list">${item.value}</div>`);
                                });
                            }   
                        }
                    });
                }
            }
        }
    });
});