from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse
from django.views.generic import View
from django.contrib.auth import get_user_model, authenticate, login
from django.contrib import messages
from django.contrib.auth.views import LogoutView, LoginView
from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _

from .forms import CustomAuthenticationForm, CustomUserCreationForm
from .models import ProfileUser

User = get_user_model()


class CustomLogoutView(LogoutView):
    def get_next_page(self):
        next_page = super(CustomLogoutView, self).get_next_page()
        messages.add_message(self.request, messages.SUCCESS, _('Success: You successfully log out!'))
        return next_page


class CustomLoginView(SuccessMessageMixin, LoginView):
    authentication_form = CustomAuthenticationForm
    template_name = 'users/login.html'
    success_message = _('Success: You were successfully logged in.')
    success_url = reverse_lazy('books:base_view')


class CustomRegistrationView(View):
    template_name = 'users/registration.html'
    form = CustomUserCreationForm
    message = _('Your account has been created! You are now able to log in.')

    def get(self, request, *args, **kwargs):
        context = {'form':self.form}
        return render(self.request, self.template_name, context)

    def post(self, request, *args,**kwargs):
        form = self.form(request.POST or None)
        if form.is_valid():
            user = form.save(commit=False)
            user.save()
            ProfileUser.objects.create(
                user = user,
                person = form.cleaned_data['select']
            )
            messages.add_message(self.request, messages.SUCCESS, self.message)
            return HttpResponseRedirect('/')

        form = self.form(request.POST)
        context = {'form':form}
        return render(self.request, self.template_name, context)


class AjaxCreationUserView(View):

    def post(self, request, *args, **kwargs):
        username = self.request.POST.get('username')
        email = self.request.POST.get('email')
        password = self.request.POST.get('password')
        person = self.request.POST.get('person')

        error_list = []
        success_list = ''

        if User.objects.filter(username=username).exists():
            error_list.append({'selector': 'id_username', 'value': _('User with this name already exist.')})
        if User.objects.filter(email=email).exists():
            error_list.append({'selector': 'id_email', 'value': _('User with this email already exist.')})
        else:
            user = User.objects.create(username=username, email=email)
            user.set_password(password)
            user.save()

            prof = ProfileUser.objects.create(user = user)
            prof.person = person[0]
            prof.save()
            success_list = 'ok'

        data = {
            'errors': error_list,
            'success': success_list
        }
        return JsonResponse(data, safe=False)


from django.template.loader import render_to_string
class AjaxModalLoginUserView(View):
    def get(self, request, *args, **kwargs):
        template_name = 'users/login_modal.html'
        form = CustomAuthenticationForm()
        context = {'form': form}

        html_form = render_to_string(template_name, context, request=self.request)
        return JsonResponse({'html_form': html_form})


class AjaxLoginUserView(View):

    def post(self, request, *args, **kwargs):
        email = self.request.POST.get('username')
        password = self.request.POST.get('password')

        error_list = []
        success_list = ''

        if not User.objects.filter(email=email).exists():
            error_list.append({'selector': 'id_username', 'value': _('Пользователь такого не существует.')})
        if not User.objects.get(email=email).check_password(password):
            error_list.append({'selector': 'id_password', 'value': _('Пароль не соответствует.')})
        else:
            user = authenticate(username=email, password=password)
            login(self.request, user)
            success_list = 'ok'
        
        success_message = _('Success: You were successfully logged in.')

        data = {
            'errors': error_list,
            'success': success_list,
            'success_message': success_message
        }
        return JsonResponse(data, safe=False)