from django.urls import path, reverse_lazy
from .views import (CustomLoginView, CustomLogoutView, CustomRegistrationView, AjaxCreationUserView, AjaxLoginUserView,
                    AjaxModalLoginUserView, )

app_name = 'users'

urlpatterns = [
    path('login/', CustomLoginView.as_view(), name='login'),
    path('logout/', CustomLogoutView.as_view(next_page = reverse_lazy('books:base_view')), name='logout'),
    path('registration/', CustomRegistrationView.as_view(), name='registration'),

    path('custom_creation_view', AjaxCreationUserView.as_view(), name='custom_creation_view'),
    path('custom_login_view', AjaxLoginUserView.as_view(), name = 'custom_login_view'),

    path('custom_modal_login_view', AjaxModalLoginUserView.as_view(), name = 'custom_modal_login_view'),
]