# from django.utils import translation
# from collections import OrderedDict
# from django.middleware.locale import LocaleMiddleware
# from django.conf import settings

# from django.contrib.sites.shortcuts import get_current_site

# class LocaleMiddleware(LocaleMiddleware):
#     def process_request(self, request):
#         supported_lang_codes = OrderedDict(settings.LANGUAGES)
#         language = None

#         # try get language code from url
#         lang_code = translation.get_language_from_path(request.path_info)
#         if lang_code is not None and lang_code in supported_lang_codes:
#             language = lang_code

#         # otherwise directly set the default language code
#         if not language:
#             language = getattr(settings, 'LANGUAGE_CODE')

#         translation.activate(language)
#         request.LANGUAGE_CODE = language




# from django.utils import translation
# from django.contrib.sites.shortcuts import get_current_site


# class SetLanguageToDomain:
#     def __init__(self, get_response):
#         self.get_response = get_response
#         # One-time configuration and initialization.

#     def __call__(self, request):
#         current_site = get_current_site(request).domain
#         if current_site == 'drenglish':
#             user_language = 'en'
#         elif current_site == 'drspanish':
#             user_language = 'es_ES'
#         else:
#             user_language = 'pt_BR'
#         translation.activate(user_language)
#         request.session[translation.LANGUAGE_SESSION_KEY] = user_language

#         response = self.get_response(request)

#         return response




from django.conf import settings
 
def switch_lang_code(path, language):
 
    # Get the supported language codes
    lang_codes = [c for (c, name) in settings.LANGUAGES]
 
    # Validate the inputs
    if path == '':
        raise Exception('URL path for language switch is empty')
    elif path[0] != '/':
        raise Exception('URL path for language switch does not start with "/"')
    elif language not in lang_codes:
        raise Exception('%s is not a supported language code' % language)
    
    # Split the parts of the path
    parts = path.split('/')

    # Add or substitute the new language prefix
    if parts[1] in lang_codes and lang_codes != 'ru':
        parts[1] = language
    else:
        parts[0] = "/" + language

    result = '/'.join(parts)
    if '/ru' in result:
        result = result.replace('/ru','')

    # Return the full new path
    return result