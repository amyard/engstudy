from django import template
from django.db.models import Q

from ..models import LessonTests
from apps.users.models import ProfileUser


register = template.Library()


@register.simple_tag
def display_individ_lesson(user):
    if user.is_anonymous:
        return []

    person = ProfileUser.objects.get(user = user).person

    if user.is_superuser:
        return LessonTests.individual_objects.all()
    elif person == 'Teacher' or person == 'T':
        return LessonTests.individual_objects.filter(owner = ProfileUser.objects.get(user = user))
    elif person == 'Student' or person == 'S':
        return LessonTests.individual_objects.filter(permit__in = ProfileUser.objects.filter(user = user))        
    else:
        return []