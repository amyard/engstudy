from django.urls import path
from .views import (LessonTestListView, LessonTestsCreateView, LessonTestDetailView, LessonTestsUpdateView, LessonTestsDeleteView, LessonTestIndividualListView, 
                    TaskTestsCreateView,
                    AjaxGetAnswerFromTask, )

app_name = 'lesson_tests'

urlpatterns = [
    path('', LessonTestListView.as_view(), name='lesson_tests_list'),
    path('individual_lesson/', LessonTestIndividualListView.as_view(), name='lesson_tests_list_individual'),

    path('lesson/create/', LessonTestsCreateView.as_view(), name='lesson_tests_create'),
    path('lesson/detail/<int:id>-<str:slug>', LessonTestDetailView.as_view(), name='lesson_tests_detail'),
    path('lesson/update/<int:pk>-<str:slug>', LessonTestsUpdateView.as_view(), name='lesson_tests_update'),
    path('lesson/delete/<int:pk>', LessonTestsDeleteView.as_view(), name='lesson_tests_delete'),

    path('task/create/', TaskTestsCreateView.as_view(), name='task_tests_create'),


    path('get_task_answer', AjaxGetAnswerFromTask.as_view(), name='get_task_answer'),
] 