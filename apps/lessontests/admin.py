from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import LessonTests, TaskTests, ResultTestModel



# class TaskInline(admin.TabularInline):
class TaskInline(admin.StackedInline):
    model = TaskTests
    extra = 0
    exclude = ['title', 'slug']



def amount_of_tasks(obj):
    return TaskTests.objects.filter(lesson = obj).count()
amount_of_tasks.short_description = _('Amount of Tasks')


@admin.register(LessonTests)
class LessonTestsAdmin(admin.ModelAdmin):
    list_display = ['username', 'title', 'permission', amount_of_tasks]
    list_display_links = ['title']
    list_filter = ['permission']
    search_fields = ['username', 'permission']
    prepopulated_fields = {'slug': ('title',)}
    inlines = [TaskInline]
    
    fieldsets = (
        ('Name', {'fields': ('title', 'slug')}),
        ('Profile Data', {'fields': ('owner', 'permission', 'permit')})
    )

    change_form_template = 'admin/lessontests/lessontests_change_list.html'

    def username(self, obj):
        return obj.owner.user.username
   



def delete_task(obj):
    return mark_safe('<a href="/admin/lessontests/tasktests/{}/delete/" style="padding: 5px 10px;background: red;color: white;">Delete</a>'.format(obj.pk))
delete_task.short_description = 'Actions'



@admin.register(TaskTests)
class TaskTestsAdmin(admin.ModelAdmin):
    list_display = ['lesson', 'position_in', 'title', 'question', 'username', 'type_question', delete_task]
    search_fields = ['lesson', 'question', 'username']
    list_filter = ['lesson']
    prepopulated_fields = {'slug': ('title',)}

    fieldsets = (
        ('Name', {'fields': ('title', 'slug')}),
        ('Question', {'fields': ('type_question', 'question', 'lesson', 'audio_file', 'position_in')}),
        ('Answers', {'fields': ('a', 'b', 'c', 'd', 'right_answer', 'custom_right_answer')})
    )

    change_form_template = 'admin/lessontests/tasktests_change_list.html'

    def username(self, obj):
        return obj.lesson.owner.user.username


@admin.register(ResultTestModel)
class ResultTestModelAdmin(admin.ModelAdmin):
    list_display = ['lesson', 'username']
    search_fields = ['lesson', 'username']
    list_filter = ['lesson']

    def username(self, obj):
        return obj.user.user.username