from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView, CreateView, DetailView, UpdateView, DeleteView, View
from django.db.models import Count
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.urls import reverse_lazy, reverse
from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _

import json

from .models import LessonTests, TaskTests, ResultTestModel, gen_slug
from .forms import LessonTestsCreateForm, LessonTestsUpdateForm, TaskTestsCreateForm
from ..users.models import ProfileUser, User

class LessonTestListView(ListView):
    model = LessonTests
    template_name='lessontests/lessontest_list.html'
    context_object_name='lessons'

    def get_queryset(self):
        return LessonTests.all_objects.all()

    def get_context_data(self, *args, **kwargs):
        context = super(LessonTestListView, self).get_context_data(*args, **kwargs)
        context['tasks'] = LessonTests.all_objects.annotate(count = Count('tasktests')).values('title', 'count')
        return context


class LessonTestIndividualListView(ListView):
    model = LessonTests
    template_name='lessontests/lessontest_list_individ.html'
    context_object_name='lessons'

    def get_queryset(self):
        return []
        # return LessonTests.individual_objects.all()
    
    def get_context_data(self, *args, **kwargs):
        context = super(LessonTestIndividualListView, self).get_context_data(*args, **kwargs)
        context['tasks'] = LessonTests.individual_objects.annotate(count = Count('tasktests')).values('title', 'count')
        return context


class LessonTestsCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = LessonTests
    template_name = 'lessontests/lessontest_create.html'
    success_message = 'Success: Lesson was created.' 
    success_url = reverse_lazy('lesson_tests:lesson_tests_list')
    form_class = LessonTestsCreateForm
    login_url = reverse_lazy('users:login')
    

    def test_func(self):
        if self.request.user.is_superuser or ProfileUser.objects.get(user = self.request.user).get_person_display() == 'Teacher':
            return True
        return False

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(LessonTestsCreateView, self).get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.instance.user = ProfileUser.objects.get(user = self.request.user)
        form.instance.slug = gen_slug(form.instance.title)
        return super().form_valid(form)


class LessonTestsUpdateView(LoginRequiredMixin, UserPassesTestMixin, UpdateView):
    form_class = LessonTestsUpdateForm
    model = LessonTests
    context_object_name = 'lesson'
    template_name = 'lessontests/lessontests_update.html'
    login_url = reverse_lazy('users:login')

    # owner or superuser
    def test_func(self):
        if self.request.user.is_superuser or self.get_object().owner.user == self.request.user:
            return True
        return False

    def get_form_kwargs(self, *args, **kwargs):
        kwargs = super(LessonTestsUpdateView, self).get_form_kwargs(*args, **kwargs)
        kwargs['user'] = self.request.user
        kwargs['pk'] = self.kwargs['pk']
        return kwargs
    
    def form_valid(self, form):
        form.instance.slug = gen_slug(form.instance.title)
        return super().form_valid(form)

    def get_success_url(self, **kwargs):
        return self.get_object().get_absolute_url()


class LessonTestsDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
    model = LessonTests
    template_name = 'lessontests/lessontest_delete.html'
    context_object_name = 'lesson'
    login_url = reverse_lazy('users:login')

    # owner or superuser
    def test_func(self):
        if self.request.user.is_superuser or self.get_object().owner.user == self.request.user:
            return True
        return False

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, 'Вы удалили урок. Поздравляю.')
        return reverse('lesson_tests:lesson_tests_list')



class LessonTestDetailView(DetailView):
    model = LessonTests
    template_name = 'lessontests/lessontests_detail.html'
    context_object_name='lesson'

    def test_func(self):
        if self.request.user.is_superuser or ProfileUser.objects.get(user = self.request.user).get_person_display() == 'Teacher':
            return True
        return False

    def get_context_data(self, *args, **kwargs):
        context = super(LessonTestDetailView, self).get_context_data(*args, **kwargs)
        context['tasks'] = TaskTests.objects.filter(lesson = self.get_object()).order_by('position_in')
        return context



def get_clear_result(data, answer):
    wrong, correct = 0, 0

    for i in data:
        if 'active' in i['result']:
            i['result'] = answer

    for i in data:
        if i['result'] == 'wrong':
            wrong +=1
        else:
            correct +=1

    return wrong, correct, data


class AjaxGetAnswerFromTask(View):

    def get(self, *args, **kwargs):
        answers = self.request.GET.get('answers')
        lesson_id = self.request.GET.get('lesson_id')
        position_in = self.request.GET.get('position_in')
        username = self.request.GET.get('username')

        result_content = self.request.GET.getlist('result_content[]', [])
        result_content = [json.loads(item) for item in result_content]

        wrong, correct, statistic = 0, 0, []     ############  дабы не было ошибок во вьюхе
        

        clear_answer = answers.replace('"','').replace('[','').replace(']','').replace(',','')
        right_answer = TaskTests.objects.get(lesson__id = lesson_id, position_in = position_in).right_answer

        if len(clear_answer) > len(right_answer):
            result = 'wrong'
        elif clear_answer[0] != right_answer and len(right_answer) == 1: 
            result = 'wrong'
        else:
            result = 'correct'

        # TODO - если result_content == 0, значит это последний вопрос и нужно опправлять статистику\
        # сохраняем только залогиненные пользователи
        if len(result_content) > 0:
            wrong, correct, clear_result = get_clear_result(result_content, result)
            if username != 'anonym' and username:
                ResultTestModel.objects.create(
                    lesson = get_object_or_404(LessonTests, pk = lesson_id),
                    user = get_object_or_404(ProfileUser, user = get_object_or_404(User, email = username)),
                    result = clear_result
                )  

                # prepare statistics
                data = ResultTestModel.objects.filter(
                    lesson = get_object_or_404(LessonTests, pk = lesson_id),
                    user = get_object_or_404(ProfileUser, user = get_object_or_404(User, email = username))
                ).values_list('created', 'result')

                timeperiod, percentage = [], []
                for i in data:
                    wrong = i[1].count('wrong')
                    correct = i[1].count('correct')
                    res = round((correct / (correct+wrong)) * 100)
                    percentage.append(res)

                    period = i[0].strftime('%d/%m/%Y')
                    timeperiod.append(period)
                statistic.append({
                    'period':timeperiod if len(timeperiod) <=10 else timeperiod[-10:], 
                    'percentage':percentage if len(percentage) <=10 else percentage[-10:]
                })

        your_result = _('your result')
        corr_res = _('corr result')
        wrong_res = _('wrong result')  
        history_result = _('History of results (last 10 results):')          
        perc_correct = _('Percent of correct results.')

        data = {
            'status': result,
            'wrong': wrong,
            'correct': correct,
            'statistic': statistic,
            'your_result': your_result, 'corr_res': corr_res, 'wrong_res': wrong_res, 'history_result':history_result, 'perc_correct':perc_correct,
        }
        return JsonResponse(data, safe=False)


class TaskTestsCreateView(CreateView):
    model = TaskTests
    form_class = TaskTestsCreateForm
    template_name = 'lessontests/tasktest_create.html'

    def get_success_url(self):
        messages.add_message(self.request, messages.INFO, 'Вы создали задание. Поздравляю.')
        return reverse('lesson_tests:lesson_tests_list')
