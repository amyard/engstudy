from django.db import models
from django.db.models import FileField
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse
from django.core.exceptions import ValidationError
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver


from django.utils.text import slugify
from pytils import translit
import pytils
import re
import uuid
import os
from model_utils.models import TimeStampedModel

from apps.users.models import ProfileUser


def gen_slug(x):
	new_slug = translit.translify(slugify(x, allow_unicode = True))
	return new_slug


class AllPermissionManager(models.Manager):
    def get_queryset(self):
        return super(AllPermissionManager, self).get_queryset().filter(permission='A')

class IndividualPermissionManager(models.Manager):
    def get_queryset(self):
        return super(IndividualPermissionManager, self).get_queryset().filter(permission='I')


# только преподы могут создвать тесты
class LessonTests(models.Model):
    
    Choices = (
        ('A', 'All'),
        ('I', 'Individual')
    )

    title = models.CharField(_('Title'), max_length=255)
    slug = models.SlugField(_('Slug'))
    owner = models.ForeignKey(ProfileUser, on_delete=models.CASCADE, related_name='lessontests')
    permission = models.CharField(max_length=10, choices=Choices, default='All')
    permit = models.ManyToManyField(ProfileUser, related_name='permit_student', blank = True, null = True)

    objects = models.Manager()
    all_objects = AllPermissionManager()
    individual_objects = IndividualPermissionManager()

    class Meta:
        verbose_name = _('Lesson Test')
        verbose_name_plural = _('Lesson Tests')
        ordering = ['-id']
        unique_together = (('slug', 'owner'),)

    def __str__(self):
        return self.title

    def get_absolute_url(self, *args, **kwargs):
        return reverse('lesson_tests:lesson_tests_detail', kwargs={'id':self.id, 'slug': self.slug})



def save_audio_path(instance, filename):
    aud_type = filename.split('.')[-1]
    new_name = '{}.{}'.format(uuid.uuid4(), aud_type)
    return 'audio/{}/{}'.format(instance.lesson.slug, new_name)



class ContentTypeFileField(FileField):
    def __init__(self, *args, **kwargs):
        self.valid_extensions = settings.CONTENT_TYPES_AUDIO
        self.max_upload_size = settings.MAX_UPLOAD_SIZE_AUDIO
        super(ContentTypeFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(ContentTypeFileField, self).clean(*args, **kwargs)
        file = data.file

        if file.size > self.max_upload_size:
            raise ValidationError(_('File too large. Size should not exceed 10 MB.'))
        
        ext = str(file).split('.')[-1]
        if not ext.lower() in self.valid_extensions:
            raise ValidationError(_('Unsupported file extension.'))
        return data         


class TaskTests(models.Model):
    Type = (
        ('choose', _('Choose correct answer')),
        ('type', _('Type answer')),
        ('move', _('Move answer')),
        ('collect', _('Collect sentence')),
        ('audio', _('Audio question'))
    )

    Answers = (
        ('a', 'Answer A'),
        ('b', 'Answer B'),
        ('c', 'Answer C'),
        ('d', 'Answer D'),
    )

    lesson = models.ForeignKey(LessonTests, related_name='tasktests', on_delete=models.CASCADE,
                                help_text = _('Выберите урок, к которому будет добавленно задание'))    
    title = models.CharField(_('Question Title'), max_length=255, null=True, blank=True,
                                help_text = _('Если оставите поле незаполенное, то будет сгенерировано название по примеру "Задание №1", где 1 - это порядковый номер отображние задания.'))
    slug = models.SlugField(_('Question Slug'), max_length=255, null=True, blank=True,
                            help_text = _('Сгенерируется автоматически.'))

    type_question = models.CharField(_('Type of question'), max_length=58, choices=Type, default='Choose correct answer')
    question = models.TextField(_('Question'))
    a = models.CharField(_('Answer A'), max_length=255, null=True, blank=True) 
    b = models.CharField(_('Answer B'), max_length=255, null=True, blank=True) 
    c = models.CharField(_('Answer C'), max_length=255, null=True, blank=True) 
    d = models.CharField(_('Answer D'), max_length=255, null=True, blank=True) 
    right_answer = models.CharField(_('Right Answer'), max_length=8, choices=Answers, default='Answer A')
    custom_right_answer = models.CharField(_('Custom Right Answer'), max_length=255, null=True, blank = True)

    audio_file = ContentTypeFileField(blank=True, null=True,
                                        help_text = _('Allowed type - .mp3, .wav, .ogg'),
                                        upload_to=save_audio_path)

    position_in = models.SmallIntegerField(_('Position in Lesson'), default=0)

    def __str__(self):
        return f'{self.id}-{self.lesson.title}'
    
    class Meta:
        verbose_name = _('Task Test')
        verbose_name_plural = _('Task Tests')
        ordering = ('title', )


    # выбираем значение отображение заданий в уроке
    
    def get_new_position(self):
        positions_used = TaskTests.objects.filter(lesson = self.lesson).values_list('position_in', flat=True).order_by().distinct()

        # если мы получили 0, добавляем как последний елемент, или первый пропущенный
        if self.position_in == 0:
            # это только первый елемент созданный в БД
            if len(positions_used) == 0:
                return 1
            else:
                # находим первое пустое значение. может быть пропущенное вначале и в самом конце
                max_value = 2 if len(positions_used) == 1 else max(positions_used)
                distinct_array = [ i for i in range(1, max_value+1) ]
                diff_list = list(set(distinct_array).symmetric_difference(positions_used))
                diff_list = list(filter(lambda x: x != 0, diff_list))
                result = min(diff_list) if diff_list else TaskTests.objects.filter(lesson = self.lesson).count()+1
                return result

        # если выбрано какое-то число, то нужно сместить все значения после нового числа на +1
        else:
            values_bigger_than_our = list(filter(lambda x: x >= self.position_in, positions_used))
            change_querysets = TaskTests.objects.filter(position_in__in = values_bigger_than_our, lesson = self.lesson)

            ids = TaskTests.objects.filter(position_in__in = values_bigger_than_our, lesson = self.lesson).values_list('id', flat=True).order_by('id')
            for numb in ids:
                query = TaskTests.objects.get(id=numb)
                TaskTests.objects.filter(id=numb).update(
                    position_in = query.position_in + 1,
                    title = self.generate_title_if_change_position(query.title, query.position_in + 1),
                    slug = gen_slug(self.generate_title_if_change_position(query.title, query.position_in + 1))
                )            
        return self.position_in

    def generate_title_if_change_position(self, title, position):
        if 'Задание' in title:
            return 'Задание №{}'.format(position)
        else:
            return title

    def generate_title(self, pos):
        if not self.title or 'Задание' in self.title:
            # pos = self.get_new_position()
            res = 'Задание №{}'.format(pos)
            return res
        return self.title

    # кастомный ответ проверка.
    # если тип вопроса "Type answer" - то количество спец символов должно быть как количество ответов
    def clean(self):
        if self.type_question == 'type' or self.type_question == 'move' or self.type_question == 'collect':
            question = self.question
            custom_right_answer = self.custom_right_answer
            question_count = len(re.findall('\.\.\.', question))
            custom_right_answer_count = len(custom_right_answer.split(','))
            if question_count != custom_right_answer_count:
                raise ValidationError(_('if you choose type of question \"Type answer / Move answer / Collect sentence" amount of \"...\" from field \"Question\" must equal of amount substrings splitted by comma from field \"Custom Right Answer\".'))
        if self.type_question == 'audio' and not self.audio_file:
            raise ValidationError(_('Please, load some audio file.'))


    def save(self, *args, **kwargs):
        self.position_in = self.get_new_position()
        self.title = self.generate_title(self.position_in)
        self.slug = gen_slug(self.title)
        super(TaskTests, self).save(*args, **kwargs)



class ResultTestModel(TimeStampedModel):
    lesson = models.ForeignKey(LessonTests, related_name='resulttests', on_delete=models.CASCADE)
    user = models.ForeignKey(ProfileUser, blank=True, null=True, on_delete=models.CASCADE, related_name='resulttests')
    result = models.TextField(_('Result'))

    def __str__(self):
        return f'{self.lesson}-{self.user}'
    
    class Meta:
        verbose_name = _('result test modal')
        verbose_name_plural = _('result test modals')



def _delete_file(path):
    if os.path.isfile(path):
        os.remove(path)

    # delete folder if it's empty to
    parent_dir = os.path.dirname(path)
    if not os.listdir(parent_dir):
        os.rmdirc(parent_dir)
    

@receiver(pre_delete, sender = TaskTests)
def delete_img_pre_delete_post(sender, instance, *args, **kwargs):
    if instance.audio_file:
        _delete_file(instance.audio_file.path)


