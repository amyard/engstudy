from django.apps import AppConfig


class LessontestsConfig(AppConfig):
    name = 'apps.lessontests'
