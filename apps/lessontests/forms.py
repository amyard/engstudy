from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import LessonTests, TaskTests, gen_slug
from apps.users.models import ProfileUser



class LessonTestsCreateForm(forms.ModelForm):

    class Meta:
        model = LessonTests
        fields = ['title', 'permission']
    
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', '')
        super(LessonTestsCreateForm, self).__init__(*args, **kwargs)

    
    def save(self, commit=True):
        instance = super(LessonTestsCreateForm, self).save(commit=False)
        instance.slug = gen_slug(instance.title)       
        if commit:
            instance.save()
        return instance


class LessonTestsUpdateForm(forms.ModelForm):

    class Meta:
        model = LessonTests
        fields = ['title', 'permission']
    
    def __init__(self, *args, **kwargs):
        self.pk = kwargs.pop('pk')
        self.user = kwargs.pop('user')
        super(LessonTestsUpdateForm, self).__init__(*args, **kwargs)

    def clean_title(self):
        title = self.cleaned_data['title']
        if LessonTests.objects.exclude(pk = self.pk).filter(title = title, user = ProfileUser.objects.get(user = self.user)).exists():
            raise forms.ValidationError(_('Урок с данным названием уже существует в вашем профиле.'))
        return title



class TaskTestsCreateForm(forms.ModelForm):

    class Meta:
        model = TaskTests
        fields = '__all__'
        exclude = ['title', 'slug']